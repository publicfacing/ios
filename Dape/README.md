# Dare iOS Epoke (Upwork Project)

iPhone Dare app for client Epoke. 
Contract started on Upwork

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

```
(on project directory)
pod install 
pod update to update pods
```

### Prerequisites

What things you need to install the software and how to install them

```
pod install
open .xcworkspace file 
build & run from XCode Menu
```

## Deployment

Submit to Apple App store from iTunes Connect Account

## Built With

* [CocoaPods](http://cocoapods.org/) - The depency manager
* [Pods](https://www.cocoacontrols.com) - The community for Pods listed below:

```
# Pods for Dare-iOS
  pod 'Parse'
  pod 'ParseLiveQuery' [Depreciated]
  pod 'ParseUI'
  pod 'GoogleMaps'
  pod 'GooglePlaces'
  pod 'ParseFacebookUtilsV4'
  pod 'DisplaySwitcher', '~> 1.0'
  pod 'EZSwipeController'
  pod 'LayerKit'
  pod 'Atlas'
  pod 'ImageSlideshow', '~> 1.1.0'
  pod 'CircularSpinner'
  pod 'GradientCircularProgress', :git => 'https://github.com/keygx/GradientCircularProgress' [Depreciated]
```

## Versioning

* Version 0.5 - Test Release.

## Authors

* **Utkarsh Kumar** - *iOS App Work* - [Ukumar246](https://github.com/Ukumar246)

## License

This project is licensed privately to be released under Epoke Company Account

