//
//  FlowTableViewController.swift
//  Dare-iOS
//
//  Created by Utkarsh Kumar on 1/21/17.
//  Copyright © 2017 Dare. All rights reserved.
//

import UIKit
import ParseUI
import GooglePlaces
import LayerKit
import KDLoadingView

private let CellIdentifierConversation = "conversation_cell_unread"
private let CellIdentifierDare = "dare_cell"
private let CellIdentifierHeader = "header_cell"
private let DareSegueIdentifer = "segue_dare_interface"
// Label Constants
private let cNoActivitiesFoundText = "No activities yet. Start a conversation, check in somewhere or Dape someone."
private let cActivitiesLoadingText = "Loading..."

struct UserActivity {
    var type:String
    var time:Date
    var parseActivity:PFObject
    var layerConversation:LYRConversation?
    var isRead:Bool
}

struct UserActivityBundle
{
    var time:Date
    var placeid:String
    var activities:[UserActivity]
    var checkin:PFObject?
}


class FlowTableViewController: UITableViewController, LYRQueryControllerDelegate{
    // MARK: - Storyboard Variables
    override var prefersStatusBarHidden: Bool{
        return false
    }
    //@IBOutlet private weak var headerView: FlowHeader!
    @IBOutlet private weak var staticEmptyStateLabel: UILabel!
    private var placesClient: GMSPlacesClient!
    private var formatter:DateFormatter!
    private var formatterPreviousDay:DateFormatter!
    
    private var layerClient: LYRClient!
    private var segueInProgress:Bool = false
    private var currentUser:PFUser!
    
    // MARK: - Data Controller Variables
    /** Array of Activies Made By the User found on the database.
        - Remark:
            Key is String stores the objectId of place -> Reference found in self.checkInActivites
            Value is array of activities in these places
    */
    var displayActivites:[UserActivityBundle]?
    
    /// Hash map of unique PlaceId <-> CheckIn Activity >> extracted from User Activities
    var checkInActivites:[String:PFObject]?{
        didSet{
            guard let checkins = checkInActivites else{
                return
            }
            for (_,act) in checkins{
                let type = act["type"] as! String
                assert(type == "checkin")
            }
        }
    }
    
    /// LIST of all activities queried from database
    private var activities:[PFObject]?
    private var selectedActivity:UserActivity?
    
    // MARK: - Refresh Controls
    private var loadingIndicator:KDLoadingView!
    
    // MARK: - Life Cycle
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        // Unsuscribe to Layer Push Updates
        NotificationCenter.default.removeObserver(self,
                                                  name: AppDelegate.layerPushNotification,
                                                  object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: AppDelegate.serverPushNotification,
                                                  object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        
        // Suscribe to Layer Push Updates
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(FlowTableViewController.notificationCenterRecievedConversationUpdate(_:)),
                                               name: AppDelegate.layerPushNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(FlowTableViewController.notificationCenterRecievedServerUpdate(_:)),
                                               name: AppDelegate.serverPushNotification,
                                               object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        loadingIndicator.startAnimating()
        // Refresh Data
        fetchUserActivitiesOnUI()
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        assert(PFUser.current() != nil, "User must be logged in to see this screen")
        assert(PFUser.current()?.objectId != nil, "user be registed on db")
        
        currentUser = PFUser.current()!
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        layerClient = appDelegate.layerClient
        assert(layerClient != nil, "LYR Client must be init at this point")
        assert(layerClient.isConnected, "LYR Client must be connected")
        assert(appDelegate.layerClientIsInitialized == true, "LYR Client must be init at this point")
        
        formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        
        formatterPreviousDay = DateFormatter()
        formatterPreviousDay.dateFormat = "E"
        
        placesClient = GMSPlacesClient.shared()
        
        let centerY:CGFloat = self.tableView.center.y - 46
        // Empty state view
        
        staticEmptyStateLabel.center = self.tableView.center
        staticEmptyStateLabel.center.y = centerY
        staticEmptyStateLabel.isHidden = false
        staticEmptyStateLabel.text = cActivitiesLoadingText
        self.view.addSubview(staticEmptyStateLabel)
        
        loadingIndicator = KDLoadingView(frame: CGRect(x: 0, y: 0, width: 60, height: 60),
                                         lineWidth: 3.0,
                                         firstColor: UIColor.clear,
                                         secondColor: UIColor.clear,
                                         thirdColor: nil,
                                         duration: 1.0)
        loadingIndicator.center = self.tableView.center
        loadingIndicator.center.y = centerY
        loadingIndicator.isHidden = true
        
        loadingIndicator.hidesWhenStopped = true
        self.view.addSubview(loadingIndicator)
    }
    
    /** Gives UserActivities mapped to a Place Object
        - Params: uniquePlacesIds object ids of unique places to filter results by
    **/
    func sortActivitiesBy(checkIns:[String:PFObject], activities list:[PFObject], layerConversations:[String:LYRConversation])->[UserActivityBundle]
    {
        //Ordered tuple collection: Place Object Id <-> User Activities (Non Check-ins) <-> CheckIn
        var temp = [UserActivityBundle]()

        // Hash Map: Unique Place <-> UserActivities
        for (place_id, a_checkin) in checkIns{
            /// User activites of type non 'check-in' that happneded in loop variable place_id
            var activities_in_this_place = list.filter({ (obj) -> Bool in
                let activity_place = obj["place"] as! PFObject
                let type = obj["type"] as! String
                return activity_place.objectId! == place_id && type != "checkin"
                // Keep only non check in objects that are filtered by Place
            });
            
            // Sort each individual activities by time
            activities_in_this_place = activities_in_this_place.sorted(by: { (first, second) -> Bool in
                let d1 = first["when"] as! Date
                let d2 = second["when"] as! Date
                return d1 > d2
            })
            
            /// Activities of non 'check-in' type the user has participated in given a place_id
            var activities = [UserActivity]();
            for act_in_place in activities_in_this_place{
                let type = act_in_place["type"] as! String
                let timestamp = act_in_place["when"] as! Date
                let readBy = act_in_place["readBy"] as! [PFUser]
                var activityIsRead = readBy.contains(currentUser)
                
                var convo:LYRConversation?
                if let cid = act_in_place["cid"] as? String{
                    convo = layerConversations[cid]
                    let lastMessageUnRead = layerConversations[cid]?.lastMessage?.isUnread ?? false
                    activityIsRead = !lastMessageUnRead
                }
                
                let act_entry = UserActivity(type: type,
                                             time: timestamp,
                                             parseActivity: act_in_place,
                                             layerConversation: convo,
                                             isRead: activityIsRead)
                activities.append(act_entry)
            }
            let checkin_time = a_checkin["when"] as! Date
            let entry = UserActivityBundle(time: checkin_time, placeid: place_id, activities: activities, checkin: a_checkin)
            temp.append(entry)
        }
        
        let sorted_display = temp.sorted {
            $0.time > $1.time
        }
        
        // Now our hash map is ready
        return sorted_display
    }
    
    /// Returns hash map of Place ObjectId to CheckInActivity type of PFObject<User Activity>
    func extractCheckInsFrom(activities list:[PFObject]) -> [String:PFObject]
    {
        let copy = list
        // Unique check in activities
        var uniqueCheckInActivities = copy.filter { (act) -> Bool in
            let type = act["type"] as! String
            return type == "checkin"
        }
        
        // Sort check in activities by time
        uniqueCheckInActivities = uniqueCheckInActivities.sorted{$0["when"] as! Date > $1["when"] as! Date}
        
        // Extract 1D array to 2D, Extract all the places
        // Hash map: PlaceId <-> Check In Activity
        var uniquePlaces = [String:PFObject]()
        for act in uniqueCheckInActivities{
            let place = act["place"] as! PFObject
            let placeid = place.objectId!
            if uniquePlaces[placeid] == nil{
                uniquePlaces[placeid] = act;    // Update based on last check in activity
            }
            
            /*
            if let existing = uniquePlaces[placeid]{
                / Update latest check in activity only
                if (act["when"] as! Date > existing["when"] as! Date){
                    print("[flow] activity \(act.objectId!)",
                          "happend before \(existing.objectId!)")
                    uniquePlaces[placeid] = act
                }
 
            }
            else{
                uniquePlaces[placeid] = act
            }
             */
        }
        
        // List all Place ID's in all of User's Activity
        var checkList = [String]()
        for act in list{
            let placeid = (act["place"] as! PFObject).objectId!
            checkList.append(placeid)
        }
        // check list contains all unique places found in user's activites 
        // Check against the 'Check In' activities we have
        for id in checkList{
            let check = uniquePlaces[id] != nil
            if !check{
                print("[flow] fatal error: lookup for place \(id) failed.",
                      "user activity is skipped from display")
            }
        }
        return uniquePlaces
    }
    
    // MARK: - Database + Query Operations
    func fetchUserActivities(callback:@escaping([PFObject]?,Error?) -> Void)
    {
        let query = PFQuery(className: "UserActivity")
        query.whereKey("participants", equalTo: currentUser)
        query.includeKeys(["createdBy",
                           "participants",
                           "place",
                           "readBy"])
        query.order(byDescending: "when")   // Descending by date
        
        // 72 Hour cutoff limit
        let now = Date()
        let cal = Calendar.current
        if let cutoffDate = cal.date(byAdding: .day, value: kQueryCutoffDayLimit, to: now){
            // Activities greater than 72 hours ago
            query.whereKey("when", greaterThanOrEqualTo: cutoffDate)
        }
        else{
            print(#function, "cutoff date not specified. Manually adding time cutoff")
            // seconds in 3 days
            let cutoffDate = now.addingTimeInterval(kQueryCutoffSecondsLimit)
            query.whereKey("when", greaterThanOrEqualTo: cutoffDate)
        }
        
        query.findObjectsInBackground { (found, err) in
            callback(found, err);
        }
    }
    
    func fetchUserLayerConversations() throws -> [String:LYRConversation]
    {
        // Query for the group conversation
        let conversationQuery = LYRQuery(queryableClass:LYRConversation.self)
        conversationQuery.limit = 500
        conversationQuery.offset = 0
        
        let found = try self.layerClient.execute(conversationQuery)
        var cache = [String:LYRConversation]();
        
        for convo in found.array as! [LYRConversation]{
            let key = convo.identifier.absoluteString
            cache[key] = convo
        }
        
        return cache
    }
    
    // MARK: Query on UI Operations
    func fetchUserActivitiesOnUI() -> Void{
        // Begin UI Updates
        loadingIndicator.center = self.view.center
        loadingIndicator.startAnimating()
        
        
        // Refresh Data
        fetchUserActivities {(results, error) in
            if let found = results, error == nil{
                self.activities = found
                self.checkInActivites = self.extractCheckInsFrom(activities: found)
                
                do{
                    let convos = try self.fetchUserLayerConversations()
                    self.displayActivites = self.sortActivitiesBy(checkIns: self.checkInActivites!,
                                                                  activities: found,
                                                                  layerConversations: convos)
                }
                catch{
                    print("[flow] error:", error.localizedDescription)
                    self.alertForError(error: error)
                }
                self.staticEmptyStateLabel.isHidden = found.count > 0
            }
            else{
                self.activities = nil
                self.checkInActivites = nil
                self.displayActivites = nil
                self.staticEmptyStateLabel.isHidden = false
            }
            self.staticEmptyStateLabel.text = cNoActivitiesFoundText
            self.tableView.reloadData()
            
            // Finish UI Updates
            self.loadingIndicator.stopAnimating()
        }
    }
    
    // MARK: - Segue Operations
    func segueToConversation(activity:UserActivity) -> Void
    {
        assert(selectedActivity != nil)
        assert(activity.type == "conversation",
               "Activity not of type conversation")
        let assert_error = "Activity ParseObj Id: \(activity.parseActivity.objectId!) does not conform to protocol."
        assert(activity.layerConversation != nil, assert_error)
        
        segueInProgress = true
        
        let dvcConvo = activity.layerConversation!
        var participantsHash = [String:PFUser]()
        
        let parseActivit = activity.parseActivity
        let participants = parseActivit["participants"] as! [PFUser]
        
        for usr in participants{
            participantsHash[usr.objectId!] = usr;
        }
        
        _ = UserManager.sharedManager.cache(users: participants)
        let convoVC = ConversationViewController(layerClient: layerClient,
                                                 layerconverstaion: dvcConvo,
                                                 participants: participantsHash,
                                                 newConversation: false)
        
        // Segue Here
        segueInProgress = false
        self.navigationController!.pushViewController(convoVC, animated: true)
    }
    
    func segueToDareInterface(activity:PFObject) -> Void
    {
        segueInProgress = false
        assert(selectedActivity != nil)
        let source = activity["createdBy"] as! PFUser
        let participants = activity["participants"] as! [PFUser]
        if source == currentUser || selectedActivity!.isRead{
            // I created the dare 
            let receiver = participants.first(where: {$0 != currentUser})       // Find the first one
            assert(receiver != nil, "Cant figure out who this dare is for")
            segueToPublicProfile(of: receiver!)
        }
        else{
            // Someone sent me this dare
            // Mark it as read
            activity.addUniqueObject(currentUser, forKey: "readBy")
            activity.saveInBackground()
            performSegue(withIdentifier: DareSegueIdentifer, sender: nil)
        }
    }
    
    func segueToPublicProfile(of targetUser:PFUser) -> Void
    {
        let storyid = PublicProfileTableViewController.storyboardId
        let vc = storyboard!.instantiateViewController(withIdentifier: storyid) as! PublicProfileTableViewController
        vc.user = targetUser
        // Set offset from Top
        let navBarHeight:CGFloat = navigationController?.navigationBar.frame.height ?? 0
        vc.navBarOffset = navBarHeight + UIApplication.shared.statusBarFrame.height
        
        // We want the public profile vc to togge our tab bar
        vc.forceHideTabBar = true
        
        // We dont want to start new convs just be pushed back here
        vc.dontStartNewConversations = false;
        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    // MARK: - UI Update Operations
    /// Refreshes the place where user has checked in on background thread
    func refreshCheckedInPlace(completion:@escaping(PFObject?,Error?) -> Void)
    {
        guard let place = currentUser["checkedIn"] as? PFObject else{
            return
        }
        
        if !place.isDataAvailable{
            // Fetch the place in background again
            place.fetchInBackground(block: { (fetchedplace, error) in
                completion(fetchedplace, error)
            })
        }
        else{
            completion(place, nil)
        }
    }
    
    @IBAction func refreshControl(_ sender: UIRefreshControl) {
        // Refresh Data
        fetchUserActivities {(results, error) in
            if let found = results, error == nil{
                print("[flow] found \(found.count) activities for user \(self.currentUser.objectId!)")
                self.activities = found
                
                self.checkInActivites = self.extractCheckInsFrom(activities: found)
                do{
                    let convos = try self.fetchUserLayerConversations()
                    self.displayActivites = self.sortActivitiesBy(checkIns: self.checkInActivites!,
                                                                  activities: found,
                                                                  layerConversations: convos)
                }
                catch{
                    print("[flow] error:", error.localizedDescription)
                    self.alertForError(error: error)
                }
                self.staticEmptyStateLabel.isHidden = found.count > 0
            }
            else{
                self.activities = nil
                self.checkInActivites = nil
                self.displayActivites = nil
                self.staticEmptyStateLabel.isHidden = false
            }
            self.staticEmptyStateLabel.text = cNoActivitiesFoundText
            sender.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    @objc func notificationCenterRecievedConversationUpdate(_ notification:Notification)
    {
        print("[flow] new app-wide notification received. type: layer")
        let params = notification.userInfo as? [String:String?]
        guard let convoId = params?["conversation_id"] else{
            return;
        }
        
        print("[flow] refresh convo id ", convoId ?? "nil")
        fetchUserActivitiesOnUI()
    }
    
    @objc func notificationCenterRecievedServerUpdate(_ notification:Notification)
    {
        let activityType:String = notification.userInfo?[AppDelegate.kParseActivityInfoKey] as? String ?? "uknown"
        guard (activityType == ParsePushType.dare.rawValue) else{
            print("[flow] activity type not supported by this view controller")
            return;
        }
        
        print("[flow] new app-wide notification received. type: \(activityType)")
        //let params = notification.userInfo as? [String:String]
        fetchUserActivitiesOnUI()
    }
    
    //MARK: - Google Places API Image Loader
    /// loads images for a given place id - gmaps
    func loadFirstPhotoForPlace(googlePlaceId id:String, callback:@escaping(UIImage?, Error?) -> Void)
    {
        let client:GMSPlacesClient = self.placesClient
        client.lookUpPhotos(forPlaceID: id) { (photos, apierror) -> Void in
            guard let firstPhotoMeta = photos?.results.first else{
                
                print("[flow] error:",
                      "\(apierror?.localizedDescription ?? "no description")")
                callback(nil, apierror)
                return
            }
            
            client.loadPlacePhoto(firstPhotoMeta, callback: { (foundImage, photoerror) -> Void in
                callback(foundImage, photoerror);
            })
        }
    }
    
    //MARK: - Table View Data Source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return displayActivites?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let activitiesInAPlace = displayActivites![section].activities
        return activitiesInAPlace.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 78.0;
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let header = tableView.dequeueReusableCell(withIdentifier: CellIdentifierHeader) as! FlowHeaderCell
        configureHeader(inSection: section, header: header)
        return header
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell:UITableViewCell!
        
        let placeIndex = indexPath.section
        let activityIndex = indexPath.row;
    
        var activities = displayActivites![placeIndex].activities
        let activity = activities[activityIndex]

        var cellIdentifier:String!
        let type = activity.type
        
        if type == "conversation"{
            cellIdentifier = CellIdentifierConversation
            let convocell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                 for: indexPath) as! ConversationCell
            configureConversationCell(conversationcell: convocell,
                                      activity: activity)
            cell = convocell
        }
        else if type == "dare"{
            cellIdentifier = CellIdentifierDare
            let darecell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                 for: indexPath) as! DareCell
            configureDareCell(dareCell: darecell, activity: activity)
            cell = darecell
        }
        else{
            print("warning: uknown activity type")
            assert(false)
        }
        
        // Configure the cell
        return cell
    }
    
    //MARK: Cell Configurations
    func configureConversationCell(conversationcell: ConversationCell, activity:UserActivity) -> Void {
        assert(activity.parseActivity.isDataAvailable, "fetch this activity first")
        assert(activity.type == "conversation", "Activity passed must be for 'Conversation' type")
        conversationcell.unreadImage.image = #imageLiteral(resourceName: "Highlighted")
        
        var toUser:PFUser?
        //TODO: Get rid of this logic
        let participants = activity.parseActivity["participants"] as! [PFUser]
        for usr in participants{
            if usr != currentUser{
                toUser = usr;
            }
        }
        
        guard let convo = activity.layerConversation else{
            conversationcell.nameLabel.text = "Could not load conversation"
            return
        }
        let unread = convo.lastMessage?.isUnread ?? false
        
        if convo.lastMessage?.sender.userID == currentUser.objectId!{
            // Sent text but no reply yet
            conversationcell.statusImage.image = #imageLiteral(resourceName: "message_sent_no_reply")
        }
        else if let _ = convo.lastMessage?.sender, unread == true{
            // Received but not opened - Last Sender exists
            conversationcell.statusImage.image = #imageLiteral(resourceName: "message_received")
        }
        else{
            conversationcell.statusImage.image = nil
        }
        
        conversationcell.unreadImage.isHidden = !unread
        conversationcell.statusImage.isHidden = unread
        conversationcell.nameLabel.text = toUser?.firstName
        return
    }
    
    func configureDareCell(dareCell cell:DareCell ,activity:UserActivity){
        assert(activity.parseActivity.isDataAvailable, "fetch this activity first")
        assert(activity.type == "dare", "Activity passed must be for 'Dare' type")
        
        let source = activity.parseActivity["createdBy"] as! PFUser
        let participants = activity.parseActivity["participants"] as! [PFUser]
        
        // Cell Decor
        cell.profileImageView.contentMode = .scaleAspectFill
        cell.profileImageView.layer.cornerRadius = cell.profileImageView.frame.width / 2;
        cell.profileImageView.clipsToBounds = true
        
        if source == currentUser || activity.isRead{
            // I created the dare
            let receiver = participants.first(where: {$0 != currentUser})       // Find the first one
            assert(receiver != nil, "Cant figure out who this dare is for")
            cell.profileImageView.file = receiver!["profilePic"] as? PFFile
            cell.profileImageView.loadInBackground()
        }
        else{
            // If dare was sent to me display the dare icon
            cell.profileImageView.image = #imageLiteral(resourceName: "Dare Logo");
        }
        return
    }
    
    func configureHeader(inSection section:Int, header:FlowHeaderCell){
        let activity = displayActivites![section]
        let checkInActivity:PFObject = activity.checkin!
        
        let place = checkInActivity["place"] as! PFObject
        assert(checkInActivity.isDataAvailable && place.isDataAvailable)
            
        // Display arrival time
        var activityTime:String = formatter.string(from: activity.time)
        let timePassed = abs(activity.time.timeIntervalSinceNow)
        if  timePassed > 3600 * 24{
            // Older than 24 hours
            activityTime = formatterPreviousDay.string(from: activity.time)
        }
        
        header.placeArrivalTimeLabel.text = activityTime
        
        let gplaceId = place["placeId"] as! String
        assert(place.isDataAvailable)
        header.placeNameLabel.text = place["name"] as? String
        header.placeAddressLabel.text = place["address"] as? String
        header.placeImageView.image = #imageLiteral(resourceName: "Empty Image");
        header.placeImageView.contentMode = .scaleAspectFill
        header.placeImageView.layer.cornerRadius = 8
        //headerView.placeImageView.frame.width / 2
        header.placeImageView.clipsToBounds = true
        
        // TODO: Memory Leaks?
        UIApplication.shared.isNetworkActivityIndicatorVisible = true;
        loadFirstPhotoForPlace(googlePlaceId: gplaceId) { (foundImage, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            header.placeImageView.image = foundImage
        }
    }
    
    // MARK: - Table View Delegates
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if segueInProgress{
            print("Warning: Segue operation in progress. Wait");
            return
        }
        
        let placeIndex = indexPath.section
        let activityIndex = indexPath.row;
        
        let activities = displayActivites![placeIndex].activities
        let activity = activities[activityIndex]
        
        // Global update
        selectedActivity = activity
        
        if activity.type == "conversation"{
            segueToConversation(activity: activity)
        }
        else if activity.type == "dare"{
            segueToDareInterface(activity: activity.parseActivity)
        }
        else{
            //TODO: Complete this
            assert(false)
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == DareSegueIdentifer{
            assert(selectedActivity != nil, "Selected Activity not set")
            let dvc = segue.destination as! DareInterfaceViewController
            dvc.activity = selectedActivity!.parseActivity
        }
    }
}

// MARK: - Subclasses
class ConversationCell: UITableViewCell {
    @IBOutlet var nameLabel:UILabel!
    @IBOutlet var unreadImage:UIImageView!
    @IBOutlet var statusImage:UIImageView!
}

class DareCell: UITableViewCell {
    @IBOutlet var profileImageView:PFImageView!
}

class FlowHeaderCell:UITableViewCell{
    @IBOutlet weak var placeImageView:PFImageView!
    @IBOutlet weak var placeNameLabel:UILabel!
    @IBOutlet weak var placeAddressLabel:UILabel!
    @IBOutlet weak var placeArrivalTimeLabel:UILabel!
}
