//
//  SettingsTableViewController.swift
//  Dare-iOS
//
//  Created by Utkarsh Kumar on 3/2/17.
//  Copyright © 2017 Epoke. All rights reserved.
//

import UIKit
import ParseFacebookUtilsV4
import SafariServices

enum RedirectType {
    case invite, review, faq, tos, learn, privacy
}

class SettingsTableViewController: UITableViewController {

    var currentUser:PFUser!
    let kSectionNotfications = 0
    let kSectionShare = 1
    let kSectionHelp = 2
    let kSectionAboutUs = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()

        currentUser = PFUser.current()!
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
    }
    
    @IBAction func unlinkButtonTapped(_ sender: UIButton){
        if PFFacebookUtils.isLinked(with: currentUser) == false
        {
            alert(message: "This user is not currently linked with any Facebook Auth", title: "Note:")
            return
        }
        
        // Unlink user 
        PFFacebookUtils.unlinkUser(inBackground: currentUser) { (succeeded, error) in
            guard succeeded, error == nil else{
                print("Facebook unlink error: ", error?.localizedDescription ?? "no description")
                self.alertForError(error: error, alternativeMessage: "Facebook unlink operation failed");
                
                return
            }
            
            self.alert(message: "User unlinked from Facebook", title: "Success")
            print("The user is no longer associated with their Facebook account.");
        }
        
    }
    
    // MARK: Actions
    @IBAction func didChangeNotification(_ sender: UISwitch){
        guard let currentInstallation = PFInstallation.current() else{
            alert(message: "This device's installtion is missing. reinstall the app.",
                  title: "Error")
            return
        }
        
        currentInstallation["allowNotifications"] = sender.isOn
        currentInstallation.saveInBackground { (success, error) in
            guard success, error == nil else{
                self.alertForError(error: error,
                                   title: "Error",
                                   alternativeMessage: "Could not save your settings at this time")
                return
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.deselectRow(at: indexPath, animated: true)
        
        let section = indexPath.section
        let row = indexPath.row
        
        print(#function, "selected ", indexPath.row)
        
        switch section {
        case kSectionShare:
            // Share
            if row == 0{
                performAction(forType: .invite)
            }
            else{
                performAction(forType: .review)
            }
            break
        case kSectionHelp:
            // Help & FAQ
            if row == 0{
                performAction(forType: .faq)
            }
            else if row == 1{
                performAction(forType: .tos)
            }
            else{
                performAction(forType: .privacy)
            }
            
            break
        case kSectionAboutUs:
            // About us
            performAction(forType: .learn)
            break
        default:
            // Do nothing
            break
        }
    }
    
    func performAction(forType: RedirectType){
        // Redirects to url
        var endPoint:String = ""
        
        switch forType {
        case .invite:
            // Share system dialog
            showShareDialog()
            return
        case .review:
            // Redirect to app store
            redirectToAppStore()
            return
        case .faq:
            endPoint = "/faq"
            break
        case .tos:
            endPoint = "/termsofservice"
            break
        case .learn:
            endPoint = "/learnmore"
            break
        case .privacy:
            endPoint = "/privacy"
            break
        }
        
        guard let url = URL(string: DapeURL + endPoint) else{
            alert(message: "could not perform this action", title: "Oops")
            return
        }
        
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
    
    func redirectToAppStore(){
        let appstore = URL(string: DapeAppStoreURL)!
        UIApplication.shared.openURL(appstore)
    }
    
    func showShareDialog(){
        let text = "Be a tap away from anyone: " + DapeURL
        let shareItems:Array = [text]
        
        let activityViewController = UIActivityViewController(activityItems: shareItems,
                                                              applicationActivities: nil)
        // ipad support
        //activityViewController.popoverPresentationController?.sourceView = self.view
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [UIActivityType.airDrop,
                                                        UIActivityType.print,
                                                        UIActivityType.addToReadingList ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
}

    
    /* MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
     */
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
