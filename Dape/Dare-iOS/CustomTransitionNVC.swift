//
//  CustomTransitionNVC.swift
//  Competish
//
//  Created by Utkarsh Kumar on 1/31/16.
//  Copyright © 2016 Kanpurwala. All rights reserved.
//

import UIKit
/*
/// Overrides the traditional animation protocol
class CustomTransitionNVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // ANIMATION Override
    override func pushViewController(viewController: UIViewController, animated: Bool) {
        /*UIView *theWindow = self.view ;
        if( animated ) {
            CATransition *animation = [CATransition animation];
            [animation setDuration:0.45f];
            [animation setType:kCATransitionPush];
            [animation setSubtype:kCATransitionFromLeft];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
            [[theWindow layer] addAnimation:animation forKey:@""];
        }
        
        //make sure we pass the super "animated:NO" or we will get both our
        //animation and the super's animation
        [super pushViewController:viewController animated:NO];
        
        [self swapButtonsForViewController:viewContro ller];
        */
        
        let theWindow = self.view;
        
        if animated{
            let animation = CATransition()
            animation.duration = 0.45;
            animation.type = kCATransitionPush
            animation.subtype = kCATransitionFade
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
            
            theWindow.layer.addAnimation(animation, forKey: "")
        }
        
        super.pushViewController(viewController, animated: false)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
 */
