import UIKit
import Atlas
import Parse

private let DareBlue:(CGFloat, CGFloat, CGFloat) = (0,31,181)
private let PlaceHolderHint = "You can text just once..."
private let PlaceHolderBrokenSingleTextRule = "Waiting for a reply ..."
class ConversationViewController: ATLConversationViewController, ATLConversationViewControllerDataSource, ATLConversationViewControllerDelegate{

    private var dareBlue:UIColor!
    private var dateFormatter: DateFormatter!
    private var currentUser:PFUser!
    private var conversationIdentifier:String!
    
    // Set User Identities for the Conversation Here:
    public var participants:[String:PFUser]!
    
    /// Conversation's recipient
    private var toParticipant:PFUser?
    /**
        This variable lets us track state of the conversation
        And Register on the database if its the first time creation
    */
    public var conversationIsNew:Bool = false
    /// This is the database activity associated with this LYRConversation
    public var asociatedActivity:PFObject?

    init(layerClient:LYRClient, layerconverstaion convo:LYRConversation, participants list:[String:PFUser], newConversation:Bool)
    {
        super.init(layerClient: layerClient)
        self.conversation = convo
        self.participants = list
        self.conversationIsNew = newConversation
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        assert(participants != nil)
        assert(participants.count >= 1)
        assert(PFUser.current()?.objectId != nil,
               "current user must have an id on db")
        
        // Delegates
        self.dataSource = self
        self.delegate = self;
        addressBarController.delegate = self
        shouldDisplayAvatarItemForAuthenticatedUser = true
        shouldDisplayAvatarItemForOneOtherParticipant = true
        displaysAddressBar = false
        marksMessagesAsRead = true
        
        // Setup the dateformatter used by the dataSource.
        dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        dareBlue = UIColor(red: 0, green: 31/255, blue: 181/255, alpha: 1)
        currentUser = PFUser.current()!
        conversationIdentifier = self.conversation.identifier.absoluteString
        
        // Find the to participant
        let firstFind = participants.first(where: {$0.key != currentUser.objectId!})
        toParticipant = firstFind!.value
        assert(toParticipant != nil)
        self.navigationItem.title = toParticipant?.firstName
        let rightBBi = UIBarButtonItem(image: #imageLiteral(resourceName: "User"),
                                       style: .plain,
                                       target: self,
                                       action: #selector(goToUserProfile(_:)) )
        
        let forceEnableMI = UIBarButtonItem(image: #imageLiteral(resourceName: "keyboard"),
                                            style: .plain,
                                            target: self,
                                            action: #selector(forceEnableMessagingInterface(_:)))
        forceEnableMI.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItems = [rightBBi,forceEnableMI]
        
        // Appearance
        self.configureUI()
        
        // Hint messages
        var placeholderText = ""
        if conversationIsNew{
             placeholderText = PlaceHolderHint
        }
        self.messageInputToolbar.textInputView.placeholder = placeholderText
        
        // Last message Rule
        let enableTextInput = !userSentLastMessage(convo: self.conversation)
        self.messageInputToolbar.isUserInteractionEnabled = enableTextInput
        self.messageInputToolbar.textInputView.placeholder = enableTextInput ? placeholderText : PlaceHolderBrokenSingleTextRule
        
        print("[conversation] identifier \(conversationIdentifier)")
        print("[conversation] message sending isEnabled: ", enableTextInput)
        
        // Single Text Rule
        let query:LYRQuery = LYRQuery(queryableClass: LYRMessage.self)
        query.predicate = LYRPredicate(property: "conversation", predicateOperator: .isEqualTo, value: self.conversation)
        
        let count = layerClient.count(for: query, error: nil)
        if count >= 2{
            self.messageInputToolbar.isUserInteractionEnabled = false
            self.messageInputToolbar.textInputView.placeholder = "Single text rule exceeded"
        }
    }
    
    func userSentLastMessage(convo:LYRConversation) -> Bool{
        guard let lastMessage = convo.lastMessage else{
            print("[conversation] last message not found")
            return false
        }
        
        let lastSender = lastMessage.sender.userID
        return lastSender == currentUser.objectId!
    }

    @objc func goToUserProfile(_ sender:UIBarButtonItem){
        let storyid = PublicProfileTableViewController.storyboardId
        let mystoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = mystoryboard.instantiateViewController(withIdentifier: storyid) as! PublicProfileTableViewController
        // We want the public screen to handle this bc we get dismissed and toggle the tab bar on
        vc.forceHideTabBar = true
        
        vc.user = self.toParticipant!
        
        let navBarHeight:CGFloat = navigationController?.navigationBar.frame.height ?? 0
        vc.navBarOffset = navBarHeight + UIApplication.shared.statusBarFrame.height
        
        vc.dontStartNewConversations = true     // We dont want to start new convs just be pushed back here
        
        self.navigationController!.pushViewController(vc, animated: true);
    }
    
    // MARK: - UI Configuration methods
    func configureUI(){
        ATLMessageCollectionViewCell.appearance().bubbleViewColor = UIColor.gray
        ATLOutgoingMessageCollectionViewCell.appearance().bubbleViewColor = dareBlue
    }
    
    // MARK: - Testing
    @objc func forceEnableMessagingInterface(_ sender:UIBarButtonItem)
    {
        print("[conversation] force enabed MI toolbar")
        self.messageInputToolbar.isUserInteractionEnabled = true
        self.messageInputToolbar.textInputView.placeholder = "send a message"
    }
    
    // MARK: - Database Operations
    func syncConversationWithDatabase(){
        assert(conversationIsNew, "Must be new to be synced")
        // Call the cloud to sync this conversation
        guard let withUserId = toParticipant?.objectId else{
            alertForError(error: nil, alternativeMessage: "Could not figure out recipient")
            print("Participants in this conversation is not equal to 2")
            return
        }
        guard let withUserPlaceId = (toParticipant!["checkedIn"] as! PFObject).objectId else{
            alertForError(error: nil, alternativeMessage: "Could not figure out recipient's check in status")
            return
        }
        
        let params = ["to": withUserId,
                      "conversationID": conversationIdentifier,
                      "to_place": withUserPlaceId];
        
        PFCloud.callFunction(inBackground: "register-chat", withParameters: params, block: { (data, err) in
            if let strData = data as? String{
                print("[☁️️ register-chat cloud] \(strData)")
                //self.alert(message: strData, title: "Conversation Synced")
                self.conversationIsNew = false
            }
            else{
                print("[☁️️ register-chat cloud] error",
                      "\(err?.localizedDescription ?? "no err description")")
                self.alertForError(error: err)
            }
        });
}

    // MARK: - ATLConversationViewControllerDelegate methods
    func conversationViewController(_ viewController: ATLConversationViewController, didSend message: LYRMessage) {
        print("Message sent! \(message.identifier.relativeString)");
        self.scrollToBottom(animated: true)
        
        // Last message
        self.messageInputToolbar.textInputView.resignFirstResponder()
        self.messageInputToolbar.isUserInteractionEnabled = false
        self.messageInputToolbar.textInputView.placeholder = PlaceHolderBrokenSingleTextRule
        
        if conversationIsNew{
            print("syncing conversation with db..")
            syncConversationWithDatabase()
        }
    }
    
    func conversationViewController(_ viewController: ATLConversationViewController, didFailSending message: LYRMessage, error: Error){
        let alertString:String = "Message failed to sent with error: \(error)"
        print(alertString)
        self.alertForError(error: error, title:"Failed to send message")
    }
    
    // MARK - ATLConversationViewControllerDataSource methods
    func conversationViewController(_ conversationViewController: ATLConversationViewController, participantFor identity: LYRIdentity) -> ATLParticipant{
        let indentityID:String = identity.userID
        if indentityID == PFUser.current()!.objectId!{
            return PFUser.current()!
        }
        
        // Check Cache for this User
        if let cachedUser = UserManager.sharedManager.cachedUserForUserID(userID: indentityID){
            return cachedUser
        }
        else{
            return participants[identity.userID]!
        }
    }

    func conversationViewController(_ conversationViewController: ATLConversationViewController, attributedStringForDisplayOf date: Date) -> NSAttributedString {
        let attributes:[NSAttributedStringKey:Any]? = [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.systemFont(ofSize: 14), NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.gray]
        return NSAttributedString(string: self.dateFormatter.string(from: date), attributes: attributes)
    }
    
    //TODO: Update these
    func conversationViewController(_ conversationViewController: ATLConversationViewController, attributedStringForDisplayOfRecipientStatus recipientStatus: [AnyHashable : Any]) -> NSAttributedString{
        if (recipientStatus.count == 0){
            return NSAttributedString(string: "")
        }
        
        return NSAttributedString(string: "")
    }
}


/*
 func conversationViewController(_ viewController: ATLConversationViewController, didSelect message: LYRMessage) {
 print("Message selected")
 
 // Chat Images
 let JPEGMessagePart = ATLMessagePartForMIMEType(message, ATLMIMETypeImageJPEG);
 if (JPEGMessagePart != nil) {
 presentImageViewControllerWithMessage(message: message);
 }
 
 let PNGMessagePart = ATLMessagePartForMIMEType(message, ATLMIMETypeImagePNG);
 if (PNGMessagePart != nil) {
 presentImageViewControllerWithMessage(message: message);
 }
 
 // HTML Links
 if let htmlMessagePart = ATLMessagePartForMIMEType(message, ATLMIMETypeTextPlain){
 let textString = String(data: htmlMessagePart.data!, encoding: .utf8)
 if (verifyUrl(urlString: textString)){
 let url:URL = URL(string: textString!)!
 UIApplication.shared.openURL(url)
 }
 return;
 }
 }
 
 func verifyUrl (urlString: String?) -> Bool {
 //Check for nil
 if urlString?.range(of: ".com") == nil{
 return false;
 }
 
 if let urlString = urlString {
 // create NSURL instance
 if let url = URL(string: urlString) {
 // check if your application can open the NSURL instance
 return UIApplication.shared.canOpenURL(url)
 }
 }
 return false
 }
 
 func presentImageViewControllerWithMessage(message:LYRMessage)
 {
 // Create image info
 //let imageInfo = JTSImageInfo();
 //imageInfo.image = loadLowResImageForMessage(message);
 
 //let imageViewer = UKConversationImageViewController(imageInfo: imageInfo, mode: .Image, backgroundStyle: .None, message: message);
 //imageViewer.showFromViewController(self, transition: .FromOffscreen);
 return
 }
 */
