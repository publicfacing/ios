//
//  PeopleViewController.swift
//  Dare-iOS
//
//  Created by Utkarsh Kumar on 1/14/17.
//  Copyright © 2017 Dare. All rights reserved.
//

import UIKit
import Parse
import ParseUI
import DisplaySwitcher
import KDLoadingView

private let AnimationDuration: TimeInterval = 0.4
private let ListLayoutStaticCellHeight: CGFloat = 99
private let GridLayoutStaticCellHeight: CGFloat = 150
private let SeguePublicProfiles:String = "segue_public_profiles"
private let CellReuseIdentifier:String = "cell_person"
private let CellGridReuseIdentifier:String = "cell_person_grid"
private let kNotCheckedStaticText:String = "You must be checked in to see people around you"
private let kNoPeopleFoundStaticText:String = "Kinda empty here. No one is checked in to this location."
class PeopleViewController: UIViewController{
    
    // MARK: Data Controllers
    var people:[PFUser]?
    var selectedPerson:PFUser?
    var currentUser:PFUser!
    /// Sync Lock Variable
    var refreshInProgress = false;
    
    // MARK: Storyboard Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet fileprivate weak var rotationButton: SwitchLayoutButton!
    @IBOutlet private weak var loadingIndicator:KDLoadingView!
    @IBOutlet private weak var emptyStateLabel:UILabel!
    var refreshControl: UIRefreshControl!

    // MARK: Collection View State Variables
    fileprivate var isTransitionAvailable = true
    fileprivate lazy var listLayout = DisplaySwitchLayout(staticCellHeight: ListLayoutStaticCellHeight, nextLayoutStaticCellHeight: GridLayoutStaticCellHeight, layoutState: .list)
    fileprivate lazy var gridLayout = DisplaySwitchLayout(staticCellHeight: GridLayoutStaticCellHeight, nextLayoutStaticCellHeight: ListLayoutStaticCellHeight, layoutState: .grid)
    fileprivate var layoutState: LayoutState = .list
   
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // Unsuscribed to server push updates
        NotificationCenter.default.removeObserver(self,
                                                  name: AppDelegate.serverPushNotification,
                                                  object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Suscribed to server push updates
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(PeopleViewController.didRecieveRemoteNotification(fromServer:)),
                                               name: AppDelegate.serverPushNotification,
                                               object: nil)
        
        loadingIndicator.startAnimating()
        refreshAndFetchPeopleNearMe { (finsihed) in
            self.loadingIndicator.stopAnimating()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(PFUser.current() != nil, "User must exist at this point")
        self.currentUser = PFUser.current()
        rotationButton.isEnabled = false;
        // Do any additional setup after loading the view.
        self.people = nil
        emptyStateLabel.isHidden = true
        
        refreshControl = UIRefreshControl()
        collectionView.alwaysBounceVertical = true
        refreshControl.addTarget(self, action: #selector(PeopleViewController.handleRefresh(refreshControl:)), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        
        // Collection View Setup
        collectionView.collectionViewLayout = listLayout
        rotationButton.isSelected = true
        collectionView.allowsSelection = true
        collectionView.scrollsToTop = true;
    }
   
    // MARK: - Actions
    func refreshAndFetchPeopleNearMe(completion:@escaping(Bool) -> Void){
        if let currentPlace = currentUser["checkedIn"] as? PFObject{
            fetchPeopleNearMe(myPlace: currentPlace, callback: { (success) in
                // Found People Near Me
                let count = self.people?.count ?? 0
                self.emptyStateLabel.text = kNoPeopleFoundStaticText
                self.emptyStateLabel.isHidden = count > 0
                self.rotationButton.isEnabled = count > 0
                self.collectionView.reloadData()
                
                completion(true)
            })
        }
        else{
            // User is not check in anywhere
            self.people = nil
            self.rotationButton.isEnabled = false
            self.collectionView.reloadData()
            emptyStateLabel.isHidden = false
            emptyStateLabel.text = kNotCheckedStaticText
            
            completion(true)
        }
    }

    @objc func handleRefresh(refreshControl: UIRefreshControl){
        if let currentPlace = currentUser["checkedIn"] as? PFObject{
            fetchPeopleNearMe(myPlace: currentPlace, callback: { (succeeded) in
                
                // Static Text Stuff
                let count = self.people?.count ?? 0
                self.emptyStateLabel.text = kNoPeopleFoundStaticText
                self.emptyStateLabel.isHidden = count > 0
                
                refreshControl.endRefreshing()
                self.collectionView.reloadData()
                self.rotationButton.isEnabled = count > 0
            });
        }
        else{
            self.rotationButton.isEnabled = false
            refreshControl.endRefreshing()
        }
    }
    
    @objc func didRecieveRemoteNotification(fromServer notification:Notification){
        let activityType:String = notification.userInfo?[AppDelegate.kParseActivityInfoKey] as? String ?? ""
        print("[people] activity type: ", activityType)
        
        guard (activityType == ParsePushType.checkin.rawValue || activityType == ParsePushType.checkout.rawValue) else{
            print("[people] activity type not supported by this view controller")
            return;
        }
        //Server push handler
        loadingIndicator.startAnimating()
        refreshAndFetchPeopleNearMe { (finished) in
            self.loadingIndicator.stopAnimating(); // End UI Updates
        }
    }
    
    
    @IBAction func buttonTapped(_ sender: AnyObject){
        if !isTransitionAvailable {
            return
        }
        
        let transitionManager: TransitionManager
        if layoutState == .list {
            
            layoutState = .grid
            transitionManager = TransitionManager(duration: AnimationDuration, collectionView: collectionView, destinationLayout: gridLayout, layoutState: layoutState)
        } else {
            
            layoutState = .list
            transitionManager = TransitionManager(duration: AnimationDuration, collectionView: collectionView, destinationLayout: listLayout, layoutState: layoutState)
        }
        transitionManager.startInteractiveTransition()
        rotationButton.isSelected = layoutState == .list
        rotationButton.animationDuration = AnimationDuration
    }
    
    // MARK: - Database Operations
    func fetchPeopleNearMe(myPlace:PFObject, callback: @escaping (Bool)->Void ){
        print("[people vc]: querying for people around me...")
        let query = PFUser.query()!
        query.whereKey("checkedIn", equalTo: myPlace)
        // Avoid seeing youself on the list
        query.whereKey("objectId",
                       notEqualTo: currentUser.objectId!);
        query.includeKeys(["profilePic",
                           "leftPic",
                           "rightPic",
                           "checkedIn"])
        query.order(byDescending: "lastCheckInTime")
        
        query.findObjectsInBackground { (users, error:Error?) in
            guard let usersNearMe = users as? [PFUser], error == nil else{
                let errormessage = error?.localizedDescription ?? "bad type cast. expected 'People'."
                print("[people vc] error: \(errormessage)")
                
                self.people = nil
                callback(false)
                return
            }
            
            print("[people vc] found \(usersNearMe.count) users near me")
            self.people = usersNearMe
            callback(true)
        }
    }
    
    //MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SeguePublicProfiles{
            
            assert(self.people != nil, "Must have found users in order to segue")
            assert(self.people!.count >= 1, "Must have found 1 or more users in order to segue")
            
            let dvc = segue.destination as! PublicProfilesSwipeVC
            dvc.people = self.people!
            dvc.selectedPerson = self.selectedPerson
        }
    }
}
//MARK: - Collection View
extension PeopleViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.people == nil{
            return 0;
        }
        return self.people!.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //print("cell: height \(cell.frame.height) width: \(cell.frame.width)")
        if layoutState == .grid{
            cell.layer.cornerRadius = 5
            return;
        }
        if let personCell = cell as? PersonCollectionCell{
            cell.layer.cornerRadius = 0
            personCell.makeImageViewsCircular()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        var cell:PersonCollectionCell!
        
        if layoutState == .list {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellReuseIdentifier, for: indexPath as IndexPath) as! PersonCollectionCell
            //cell.frame.size = CGSize(width: 375, height: 99)
        } else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellGridReuseIdentifier, for: indexPath as IndexPath) as! PersonCollectionCell
            //cell.frame.size = CGSize(width: 165, height: 165)
        }
        
        
        assert(self.people != nil);
        let person = self.people![indexPath.row]
        
        let fullName = person["name"] as? String
        let fName = fullName?.components(separatedBy: " ").first
        
        cell.profileNameText = fName ?? "--"
        cell.profileDetailText = person["about"] as? String

        cell.profileImage = person["profilePic"] as? PFFile

        if let lastSeenTime = person["lastCheckInTime"] as? Date{
            let timeDiff = abs(lastSeenTime.timeIntervalSinceNow)
            var statusString:String?
            
            if timeDiff > 2*60*60{  // +2 Hrs
                statusString = "+2 Hrs"
            }
            else if timeDiff > 1*60*60{ // +1 hour
                statusString = "+1 Hrs"
            }
            else if timeDiff > 30*60{ // 30 Min
                statusString = "30 Mins"
            }
            else if timeDiff > 15*60{ // 15 Min
                statusString = "15 Mins"
            }
            else if timeDiff > 5*60{ // 5 Min
                statusString = "5 Mins"
            }
            else{       //just arrived
                statusString = "Recent"
            }
            
            cell.profileLastSeen = statusString
        }
        else{
            cell.profileLastSeen = nil;
        }
        
        return cell;
    }
 
    func collectionView(_ collectionView: UICollectionView, transitionLayoutForOldLayout fromLayout: UICollectionViewLayout, newLayout toLayout: UICollectionViewLayout) -> UICollectionViewTransitionLayout {
        let customTransitionLayout = TransitionLayout(currentLayout: fromLayout, nextLayout: toLayout)
        //collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        return customTransitionLayout
    }
    
    func collectionView(_ collectionView: UICollectionView, targetContentOffsetForProposedContentOffset proposedContentOffset: CGPoint) -> CGPoint {
        
        collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        collectionView.reloadData();
        return CGPoint(x: 0, y: 0)
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        self.selectedPerson = people![indexPath.row]
        performSegue(withIdentifier: SeguePublicProfiles, sender: self)
    }
    
    // MARK: - Scroll View Delegates
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isTransitionAvailable = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        isTransitionAvailable = true
    }

}

class PersonCollectionCell: UICollectionViewCell {
    @IBOutlet private weak var profileImageView: PFImageView?
    @IBOutlet private weak var profileDetailLabel: UILabel?
    @IBOutlet private weak var profileTitleLabel: UILabel?
    
    @IBOutlet private weak var profileTimeLabel: UILabel?
    
    var profileDetailText:String?{
        didSet{
            self.profileDetailLabel?.text = profileDetailText
        }
    }
    
    var profileNameText:String?{
        didSet{
            self.profileTitleLabel?.text = profileNameText
        }
    }
    
    var profileImage:PFFile?{
        didSet{
            if profileImage == nil{
                print("no profile picture for this cell")
                self.profileImageView?.image = #imageLiteral(resourceName: "Empty Image");
            }
            else{
                self.profileImageView?.file = profileImage
                self.profileImageView?.loadInBackground();
            }
        }
    }
    
    var profileLastSeen:String?{
        didSet{
            self.profileTimeLabel?.text = profileLastSeen
        }
    }
    
    func makeImageViewsCircular(){
        if let imgView = self.profileImageView{
            imgView.layer.cornerRadius = imgView.frame.width / 2
        }
    }
}


/*MARK: - Table View
extension PeopleViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.people == nil{
            return 0;
        }
        
        return self.people!.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "people_cell", for: indexPath) as! PeopleCell
        
        let person = self.people![indexPath.row]
        
        cell.profileNameText = person["name"] as? String
        cell.profileDetailText = person["about"] as? String
        
        cell.profileImage = person["profilePic"] as? PFFile
        
        if let lastSeenTime = person["lastCheckInTime"] as? Date{
            let timeDiff = lastSeenTime.timeIntervalSinceNow
            cell.profileLastSeen = timeDiff
        }
        else{
            cell.profileLastSeen = 0
        }
        
        cell.profileImageView.layer.cornerRadius = cell.profileImageView.frame.width / 2
        cell.updateConstraints()
        cell.setNeedsLayout()
        
        return cell
    }
}
*/

class PeopleCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: PFImageView!
    @IBOutlet weak var profileDetailLabel: UILabel!
    @IBOutlet weak var profileTitleLabel: UILabel!
    
    @IBOutlet weak var profileTimeLabel: UILabel!

    var profileDetailText:String?{
        didSet{
            self.profileDetailLabel.text = profileDetailText
        }
    }
    
    var profileNameText:String?{
        didSet{
            self.profileTitleLabel.text = profileNameText
        }
    }
    
    var profileImage:PFFile?{
        didSet{
            if profileImage == nil{
                print("no profile picture for this cell")
                self.profileImageView.image = #imageLiteral(resourceName: "Empty Image");
            }
            else{
                self.profileImageView.file = profileImage
                self.profileImageView.loadInBackground();
            }
        }
    }
    
    var profileLastSeen:String?{
        didSet{
            self.profileTimeLabel.text = profileLastSeen
        }
    }
    
}
