//
//  ProfileViewController.swift
//  Dare-iOS
//
//  Created by Utkarsh Kumar on 12/21/16.
//  Copyright © 2016 Dare. All rights reserved.
//

import UIKit
import ParseFacebookUtilsV4
import FBSDKLoginKit
import ParseUI
import LayerKit
import ImageSlideshow
import KDLoadingView

private let PopoverSocialMediaOptionsId = "popover_choose_social_media"
private let PopoverSocialMediaSegueId = "popover_choose_social_media"
private let SeguePresentLoginViewController = "segue_present_login"
/// Minimum number of social media accounts needed to save
private let kSocialMediaMinCount = 1

public let kUserSocialMedia1 = "instagram"
public let kUserSocialMedia2 = "twitter"
public let kUserSocialMedia3 = "snapchat"
public let kUserSocialMedia4 = "whatsapp"
public let kUserSocialMedia5 = "otherSM"
public let kUserSocialMediaList:[String] = [kUserSocialMedia1,kUserSocialMedia2,
                                     kUserSocialMedia3, kUserSocialMedia4,
                                     kUserSocialMedia5]

class ProfileTableViewController: UITableViewController, UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UIPopoverPresentationControllerDelegate{

    // MARK: Storyboard Outlets
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var leftImage: PFImageView!
    @IBOutlet weak var mainImage: PFImageView!
    @IBOutlet weak var rightImage: PFImageView!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var aboutTextView:UITextView!
    @IBOutlet weak var rbbi: UIBarButtonItem!
    private var lbbi:UIBarButtonItem!
    private var mainTabBarController:MainTabBarController!
    
    // MARK: Cells
    @IBOutlet weak var firstCell: SocialMediaCell!
    @IBOutlet weak var secondCell: SocialMediaCell!
    @IBOutlet weak var thirdCell: SocialMediaCell!
    @IBOutlet weak var fourthCell: SocialMediaCell!
    @IBOutlet weak var fifthCell: SocialMediaCell!
    private var allCells:[SocialMediaCell]!
    fileprivate var selectedCell:SocialMediaCell?
    
    
    fileprivate var currentUser:PFUser!
    private var editMode:Bool = false{
        didSet{
            rbbi.isEnabled = editMode;
            rbbi.title = (editMode) ? "Save" : nil;
            replaceLeftBBi(editingEnabled: editMode)
            if (editMode){
                let _ = enableEditingUI();
            }
            else{
                let _ = disableEditingUI();
            }
            //Tab Bar is disabled
            mainTabBarController.tabBar(isEnabled: !editMode)
        }
    }
    private var selectedImageView:PFImageView?
    private let kAvailableSocialMediaList = ["facebook", "instagram",
                                            "snapchat", "whatsapp",
                                            "twitter", "pinterest",
                                            "tumblr", "google_plus"]
    
    private var layerClient:LYRClient!
    private var fullScreenImages:ImageSlideshow!
    
    // MARK: - Refresh Controls
    private var loadingIndicator:KDLoadingView!
    
    // MARK: - Lifecycle
    override func viewDidLoad(){
        super.viewDidLoad()
        
        // Views Setup
        makeViewCircular(views: [leftImage, mainImage, rightImage]);
        rbbi.isEnabled = false;
        rbbi.title = nil;
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        layerClient = appDelegate.layerClient
        
        mainTabBarController = self.tabBarController as? MainTabBarController
        // Image Slide show
        fullScreenImages = ImageSlideshow(frame: self.view.bounds)
        
        // Load Profile Data
        currentUser = PFUser.current()!
        assert(currentUser != nil, "Must be signed in to access this page");
        
        allCells = [firstCell, secondCell, thirdCell, fourthCell, fifthCell]
        
        for (index, cell) in allCells.enumerated(){
            cell.tag = index
            let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileTableViewController.moreSocialMediaTapped(_:)))
            tap.numberOfTapsRequired = 1
            cell.accountImageView.isUserInteractionEnabled = false
            cell.accountImageView.tag = index
            cell.accountImageView.addGestureRecognizer(tap)
        }
        
        // Warning: load Profile disables edit mode & crashes if enabled
        loadProfile(user: currentUser)
        
        editMode = currentUser.isNew
        replaceLeftBBi(editingEnabled: editMode)
        
        loadingIndicator = setupLoadingIndicator()
    }
    
    func setupLoadingIndicator() -> KDLoadingView {
        let indicator = KDLoadingView(frame: CGRect(x: 0, y: 0, width: 60, height: 60),
                                         lineWidth: 3.0,
                                         firstColor: self.view.tintColor,
                                         secondColor: UIColor.darkGray,
                                         thirdColor: nil,
                                         duration: 1.0)
        indicator.center = view.center
        
        indicator.hidesWhenStopped = true
        view.addSubview(indicator)
        return indicator
    }
    
    //MARK: UI Actions
    func makeViewCircular(views:[UIView]) -> Void {
        for aView in views{
            aView.layer.masksToBounds = true;
            aView.layer.cornerRadius = aView.frame.width / 2;
            aView.clipsToBounds = true;
        }
        
        return
    }
    
    func replaceLeftBBi(editingEnabled:Bool){
        if editingEnabled{
            // Add a cancel Button
            let cancelBBi = UIBarButtonItem(title: "Cancel",
                                            style: .plain,
                                            target: self,
                                            action: #selector(ProfileTableViewController.cancelEditingAction(_:)))
            lbbi = cancelBBi
            navigationItem.leftBarButtonItem = cancelBBi
        }
        else{
            let editButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Edit"),
                                             style: .plain,
                                             target: self,
                                             action: #selector(ProfileTableViewController.editAction(_:)))
            
            lbbi = editButton
            navigationItem.leftBarButtonItem = editButton
        }
    }
    
    /// Disables editing mode on UI elements
    func disableEditingUI() -> Bool{
        assert(editMode == false, "Cannot disable UI while in edit mode");
        
        aboutTextView.isEditable = false;
       
        for cell in allCells{
            cell.disableEditing()
        }

        leftImage.isUserInteractionEnabled = true
        rightImage.isUserInteractionEnabled = true
        mainImage.isUserInteractionEnabled = true
        
        return true;
    }
    
    /// Enables editing mode on UI elements
    func enableEditingUI() -> Bool{
        assert(editMode == true, "Cannot disable UI while out of edit mode");
        
        aboutTextView.isEditable = true;
        for cell in allCells{
            cell.enableEditing()
        }
        
        leftImage.isUserInteractionEnabled = true
        rightImage.isUserInteractionEnabled = true
        mainImage.isUserInteractionEnabled = true
        
        return true;
    }
    
    /// Releases Images, etc on this view controller
    func releaseData(){
        leftImage.image = nil
        leftImage.file = nil
        
        mainImage.file = nil
        mainImage.image = nil
        
        rightImage.image = nil
        rightImage.file = nil
    }
    
    /// Populates UI with information from Database
    func loadProfile(user:PFUser) -> Void{
        aboutTextView.textColor = UIColor.black
        aboutTextView.text = user["about"] as? String;
        if user["about"] == nil{
            // Placeholder for about when empty
            aboutTextView.text = "In a few words, what’s up?"
            aboutTextView.textColor = UIColor.lightGray
        }
        let fullName = user["name"] as? String
        let fName = fullName?.components(separatedBy: " ").first
        nameLabel.text = fName;
        
        // Image fields
        let emptyImage = #imageLiteral(resourceName: "Add Image");
            leftImage.image = emptyImage
            rightImage.image = emptyImage
            mainImage.image = emptyImage
        
        leftImage.file = user["leftPic"] as? PFFile
        rightImage.file = user["rightPic"] as? PFFile
        mainImage.file = user["profilePic"] as? PFFile
        
        mainImage.loadInBackground()
        rightImage.loadInBackground()
        leftImage.loadInBackground()
        
        // Disable Editing
        let _ = disableEditingUI()
        
        // Load permissions
        guard let currentSocialMedia = currentUser["currentSocialMedia"] as? [String] else {
            print("[profile] missing entitlements info");
            alert(message: "Please add at least one Social Media and share",
                  title: "Info")
            return;
        }
        
        var publicEntitlements:[String]
        var dareEntitlements:[String]
        
        let empty = [String]()
        var temp:[String]?
        
        temp = currentUser["publicEntitlements"] as? [String]
        publicEntitlements = temp ?? empty
        
        temp = currentUser["dareEntitlements"] as? [String]
        dareEntitlements = temp ?? empty
        
        for (index, sm) in currentSocialMedia.enumerated(){
            if index >= allCells.count{
                print("[profile] Warning cannot have more than 5 sm in 'currentSocialMedia'")
                continue
            }
            let cell = allCells[index]
            
            let smKey = kUserSocialMediaList[index]
            let smInfo = currentUser[smKey] as? String
            let publicenable = publicEntitlements.contains(sm)
            let dareenable = dareEntitlements.contains(sm)
            
            cell.setAccountInfo(info: smInfo,
                                dareEnabled: dareenable,
                                publicEnabled: publicenable)
            cell.setAccountType(type: sm)
        }
        
        return
    }
    
    //MARK: - Actions & Gestures
    @IBAction func editAction(_ sender: UIBarButtonItem?){
        // Handlers
        weak var weakSelf = self
        
        let logoutHandler = { (action:UIAlertAction) in
            weakSelf?.editMode = false
            weakSelf?.releaseData()
            
            // UI indicator
            weakSelf?.loadingIndicator.startAnimating()
            
            // DEBUG / TESTING for Simulator
            if UIDevice.isSimulator{
                print(#function, " skipping auto checkout.")
                self.logoutFromApplication(weakSelf: weakSelf)
                return;
            }
            
            // Checkout first
            weakSelf?.checkout({ (checkedout, cError) in
                guard cError == nil else{
                    weakSelf?.loadingIndicator.stopAnimating()
                    weakSelf?.alert(message: cError?.localizedDescription ?? "retry again",
                                    title: "Error")
                    
                    return
                }
                
                // Logout from the app
                self.logoutFromApplication(weakSelf: weakSelf)
            })
            
            
        }
        
        let editHandler = { (action:UIAlertAction) in
            self.editMode = true;
        }
        
        // Actions
        let logoutAction = UIAlertAction(title: "Logout", style: .destructive, handler: logoutHandler);
            logoutAction.setValue(#imageLiteral(resourceName: "logout"), forKey: "image")
        let editAction = UIAlertAction(title: "Edit Profile", style: .default, handler: editHandler);
            editAction.setValue(#imageLiteral(resourceName: "Edit"), forKey: "image")
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil);
        
        // Display Actions
        showActionSheet(message: nil, title: "Options", actions: [editAction, logoutAction, cancelAction])
    }
    
    @IBAction func cancelEditingAction(_ sender:UIBarButtonItem?){
        currentUser.fetchInBackground { (obj, error) in
            guard let fetched = obj as? PFUser, error == nil else{
                self.alertForError(error: error)
                return
            }
            
            self.currentUser = fetched
            //TODO: Restore UI with old data
            self.editMode = false
            self.loadProfile(user: fetched)
            
        }
    }
    
    func logoutFromApplication(weakSelf:ProfileTableViewController?){
        // Proceed to log out user
        weakSelf?.logout({ (loggedout, lerror) in
            guard lerror == nil else{
                weakSelf?.loadingIndicator.stopAnimating() // End UI updates
                weakSelf?.alert(message: lerror?.localizedDescription ?? "retry again",
                                title: "Error")
                return
            }
            
            // Segue Back
            DispatchQueue.main.async {
                let presented = weakSelf?.presentedViewController
                if presented != weakSelf{
                    print(#function, "alert is showing")
                    presented?.dismiss(animated: true, completion: nil)
                    
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.8) {
                        weakSelf?.loadingIndicator.stopAnimating() // End UI updates
                        weakSelf?.performSegue(withIdentifier: SeguePresentLoginViewController,
                                               sender: nil)
                    }
                }
                else{
                    weakSelf?.loadingIndicator.stopAnimating() // End UI updates
                    weakSelf?.performSegue(withIdentifier: SeguePresentLoginViewController,
                                           sender: nil)
                }
            }
        })
    }
    
    func resetInstallation() -> Bool{
        // Attach to installation
        if let installation = PFInstallation.current(){
            installation.remove(forKey: "user")
            installation.saveInBackground()
            return true
        }
        
        print(#function, "error missing installation. cannot bind user")
        return false
    }
    
    @IBAction func saveChangesAction(_ sender: UITabBarItem?) {
        // UI Updates
        sender?.title = "Saving...";
        sender?.isEnabled = false;
        loadingIndicator.startAnimating()
        
        // Core save operations
        currentUser["about"] = aboutTextView.text ?? "";
        
        currentUser.remove(forKey: "currentSocialMedia")
        
        // Index though all social media info provided
        var smInfoCount = 0
        for (index, cell) in allCells.enumerated(){
            guard let fieldName = cell.getFieldName() else{
                continue;
            }
            if index >= allCells.count{
                print(#function, "warning: exceeds cell list count")
                continue
            }
            
            currentUser.addUniqueObject(fieldName, forKey: "currentSocialMedia")
            if let smInfo = cell.getAccountInfo(){
                let smKey = kUserSocialMediaList[index]
                currentUser[smKey] = smInfo
                if smInfo.isEmpty == false{
                    smInfoCount += 1
                }
            }
        }
        
        let publicSm = currentUser["publicEntitlements"] as? [String]
        let dareSm = currentUser["dareEntitlements"] as? [String]
        let smShareCount = (publicSm?.count ?? 0) + (dareSm?.count ?? 0)
        
        let saveConditionFailed:Bool = smShareCount < kSocialMediaMinCount || smInfoCount < kSocialMediaMinCount
        
        if saveConditionFailed{
            
            // Min number of social media account not met
            self.loadingIndicator.stopAnimating()
            sender?.title = "Save"
            sender?.isEnabled = true
            
            alert(message: "Please provide at least 1 social media and share before saving",
                  title: "Oops")
            return
        }
        
        currentUser.saveInBackground { (saved, dbError) in
            // End UI Updates
            self.editMode = false;      // Done Editing
            self.loadingIndicator.stopAnimating()
            
            if saved && dbError == nil{
                // No alert on successful save
                //self.alert(message: "Changes saved", title: "Success")
            }
            else{
                self.alertForError(error: dbError, title: "Error Saving Data")
            }
        }
    }
    
    
    // MARK: Database Operations
    func logout(_ handler:@escaping(Bool, Error?) -> Void){
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        
        weak var weakSelf = self
        
        FBSDKAccessToken.setCurrent(nil);
        PFUser.logOutInBackground(block: { (error) in
            guard error == nil else{
                handler(false, error)
                return
            }
            //print("[logout] parse deauthenticated")
            
            // Unset Installation Binding
            weakSelf?.resetInstallation()
            
            // Logout of Chat
            weakSelf?.layerClient.deauthenticate(completion: handler)
        })
    }
    
    func checkout(_ completion:@escaping(Bool, Error?) -> Void){
        // ignore notifications for this action
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.ignoreNotifications = true
        
        // Cloud checkout function
        PFCloud.callFunction(inBackground: "check-out", withParameters: nil) {(data, error) in
            completion(true, error)
        }
    }
    
    func remove(imageView: PFImageView, imageFieldName:String){
        
        if currentUser[imageFieldName] == nil{
            print(#function, "this image does not exist")
            return
        }
        
        // Remove this image
        imageView.file = nil
        imageView.image = #imageLiteral(resourceName: "Add Image")
        currentUser.remove(forKey: imageFieldName)
    }
    
    
    // MARK: Image taps
    @IBAction func leftImageTapped(_ sender: UITapGestureRecognizer){
        guard (editMode == true) else{
            presentFullscreenImages(currentPhotoIndex: 0)
            return
        }
        
        // We are in Editing Mode
        actionSheetForImagePickerOptions(chosenImageView: leftImage)
    }
    
    @IBAction func mainImageTapped(_ sender: UITapGestureRecognizer){
        if !editMode{
            presentFullscreenImages(currentPhotoIndex: 1)
            return
        }
        
        // We are in Editing Mode
        actionSheetForImagePickerOptions(chosenImageView: mainImage)
    }
    
    @IBAction func rightImageTapped(_ sender: UITapGestureRecognizer){
        if !editMode{
            presentFullscreenImages(currentPhotoIndex: 2)
            return
        }
        
        // We are in Editing Mode
        actionSheetForImagePickerOptions(chosenImageView: rightImage)
    }

    // MARK: Social Media Buttons
    @objc func moreSocialMediaTapped(_ sender: UITapGestureRecognizer){
        let tag = sender.view!.tag
        selectedCell = allCells[safe: tag]
        
        guard let removeInfo = selectedCell?.getFieldName() else{
            print("[profile] warning: no field name exists for this cell")
            return
        }
        
        // Remove the Dare & Public Entitlements for this SM for this user
        if let dareEntitlements = currentUser["dareEntitlements"] as? [String]{
            // Remove from Dare
            let filtered = dareEntitlements.filter{$0 != removeInfo}
            currentUser["dareEntitlements"] = filtered
        }
        if let publicEntitlements = currentUser["publicEntitlements"] as? [String]{
            // remove from Public
            let filtered = publicEntitlements.filter{$0 != removeInfo}
            currentUser["publicEntitlements"] = filtered
        }

        performSegue(withIdentifier: PopoverSocialMediaSegueId, sender: self)
    }
    
    // MARK: Entitlement Buttons
    @IBAction func publicEntitlementButtonTapped(_ sender: UIButton){
        guard editMode else{
            return
        }
        
        let tBtn = sender as! ToggleButton;
        let update = !tBtn.isTurnedOn;
        
        let superView = tBtn.superview?.superview as! SocialMediaCell
        superView.setPublic(enabled: update)
        let fieldName = superView.getFieldName()!
        
        if (update){
            // Add SM Field to public and dare entitlements
            currentUser.addUniqueObject(fieldName, forKey: "publicEntitlements")
            currentUser.addUniqueObject(fieldName, forKey: "dareEntitlements")
        }
        else{
            // Turn off this entitlement
            if let publicEntitlements = currentUser["publicEntitlements"] as? [String]{
                let filtered = publicEntitlements.filter{$0 != fieldName}
                currentUser["publicEntitlements"] = filtered
            }
        }
    }
    
    @IBAction func dareEntitlementButtonTapped(_ sender: UIButton){
        guard editMode else{
            return
        }
        
        let tBtn = sender as! ToggleButton;
        let update = !tBtn.isTurnedOn
        
        let superView = tBtn.superview?.superview as! SocialMediaCell
        superView.setDare(enabled: update)
        let fieldName = superView.getFieldName()!
        
        if (update){
            // Add SM Field to dare entitlements
            currentUser.addUniqueObject(fieldName, forKey: "dareEntitlements")
        }
        else{
            // Remove from dare entitlement
            if let dareEntitlements = currentUser["dareEntitlements"] as? [String]{
                let filtered = dareEntitlements.filter{$0 != fieldName}
                currentUser["dareEntitlements"] = filtered
            }
        }
    }
    
    // MARK: - Image Picker & Presenter Related
    func presentFullscreenImages(currentPhotoIndex:Int)
    {
        let empty = #imageLiteral(resourceName: "Empty Image")
        var availableImages = [ParseSource]()
        
        /// Add users pictures to carousel
        for imgfield in ["leftPic","profilePic","rightPic"]
        {
            if let limg = currentUser[imgfield] as? PFFile{
                let psrc = ParseSource(file: limg, placeholder: empty)
                availableImages.append(psrc)
            }
        }
    
        if availableImages.count > 0{
            self.fullScreenImages.setImageInputs(availableImages)
            self.fullScreenImages.setCurrentPage(currentPhotoIndex, animated: true)
            self.fullScreenImages.presentFullScreenController(from: self)
        }
        else{
            self.alert(message: "No images available to show.")
        }
    }
    
    func actionSheetForImagePickerOptions(chosenImageView imgView:PFImageView)
    {
        // We are in Editing Mode
        let photoLibrary = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            self.pickImageFromController(activeImageView: imgView, sourceType: .photoLibrary)
        }
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.pickImageFromController(activeImageView: imgView, sourceType: .camera)
        }
        
        let deleteAction = UIAlertAction(title: "Delete", style: .default) { (action) in
            
            var imageKey:String
            if imgView == self.leftImage{
                imageKey = "leftPic"
            }
            else if imgView == self.rightImage{
                imageKey = "rightPic"
            }
            else{
                // Main image was selected -> Do nothing
                assert(imgView == self.mainImage, "Logic assertion")
                return
            }
            
            self.remove(imageView: imgView, imageFieldName: imageKey)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        let actionsheetMessage:String
        var actions:[UIAlertAction]
        if imgView != mainImage{
            // All actions are available for image views (besides main image view)
            actions = [photoLibrary, cameraAction, deleteAction, cancelAction]
            actionsheetMessage = "Choose an option below"
        }
        else{
            actions = [photoLibrary, cameraAction, cancelAction]
            actionsheetMessage = "Chooe an option below. \nNote: You cannot remove the main image"
        }
        
        showActionSheet(message: actionsheetMessage,
                        title: "Options",
                        actions: actions)
    }
    
    /// Launches Image Picker
    func pickImageFromController(activeImageView:PFImageView, sourceType:UIImagePickerControllerSourceType)
    {
        selectedImageView = activeImageView;
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self;
        
        imagePicker.allowsEditing = true
        imagePicker.sourceType = sourceType;
        
        present(imagePicker, animated: true, completion: nil);
    }
    
    /// Image was picked from User Photo Library
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        dismiss(animated: true, completion: nil)
        
        guard let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage else {
            return
        }
        
        // Picked Image is here
        selectedImageView?.image = pickedImage;     // Update UI
        
        let pickedImageData = UIImageJPEGRepresentation(pickedImage, 1.0);
        assert(pickedImageData != nil , "IMAGE Data Must never be nil");
        let pickedImageFile = PFFile(name: "\(currentUser.objectId!)_img.jpg", data: pickedImageData!)
        
        if (selectedImageView == leftImage){
            currentUser["leftPic"] = pickedImageFile;
        }
        else if selectedImageView == rightImage{
            currentUser["rightPic"] = pickedImageFile;
        }
        else{
            assert(selectedImageView == mainImage, "Logic assertion");
            currentUser["profilePic"] = pickedImageFile;
        }
        
        // Save the data
        loadingIndicator.startAnimating()
        currentUser.saveInBackground(block: { (saved, dbError:Error?) in
            self.loadingIndicator.stopAnimating()
            
            if (saved == true && dbError == nil){
                // No alert on successful save
                //self.alert(message: "Saved Image", title:"Success")
            }
            else{
                // Revert Changes
                self.selectedImageView?.file = nil
                self.selectedImageView?.image = #imageLiteral(resourceName: "Add Image")
                self.alertForError(error: dbError, title: "Error uploading this image")
            }
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil);
    }
    
    // MARK: - TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder(); return true;
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "In a few words, what’s up?"
            textView.textColor = UIColor.lightGray
        }
    }
    
    // MARK: - Popover Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == PopoverSocialMediaSegueId{
            let popoverViewController = segue.destination as! AdditionalSocialMediaViewController
            popoverViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            
            popoverViewController.popoverPresentationController!.sourceView = self.view
            popoverViewController.popoverPresentationController!.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0);
            
            popoverViewController.popoverPresentationController?.permittedArrowDirections = .init(rawValue: 0)
            popoverViewController.popoverPresentationController?.containerView?.alpha = 1;
            
            popoverViewController.popoverPresentationController!.delegate = self
            
            popoverViewController.delegate = self
            
            // Available Fields
            var availableFields = [String]()
            var usedFields = [String]()
            for cell in allCells{
                let fieldName = cell.getFieldName()!
                usedFields.append(fieldName)
            }
            for field in kAvailableSocialMediaList{
                let taken = usedFields.contains(field)
                if !taken{
                    availableFields.append(field)
                }
            }
            
            popoverViewController.availableSocialMediaFields = availableFields
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension ProfileTableViewController: SocialMediaChosen{
    func didChooseSocialMedia(smFieldName: String){
        // Set the Account type to new one
        selectedCell?.setAccountType(type: smFieldName)
        selectedCell?.setAccountInfo(info: nil,
                                     dareEnabled: false,
                                     publicEnabled: false)
        
        
    }
}

extension ProfileTableViewController{
    func loadDataFromFacebook(){
        print("[test] loading data from facebook...");
        let fbsdkUserFields:String = "name,location,email,id,gender,birthday,about,cover,hometown,website,relationship_status,link";
        let request = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": fbsdkUserFields])
        
        // Start Facebook Graph Request
        request!.start(completionHandler: { (connection, result, error) in
            guard error == nil else{
                self.alertForError(error: error, title: "Facbook Error Loading Data")
                return;
            }
            
            // Result is a dictionary with the user's Facebook data
            let userData:NSDictionary = result as! NSDictionary
            
            print("User Facebook Dump:\t")
            print(userData)
            
            return
        }); // End of Facebook Graph Request
    }
}

/// Custom Extention for Toggle Button
class ToggleButton:UIButton{
    var isTurnedOn:Bool = true
}

class SocialMediaCell: UITableViewCell {
    private var fieldName:String?
    @IBOutlet private var leftEntitlementButton:ToggleButton!       // Public Button
    @IBOutlet private var rightEntitlementButton:ToggleButton!      // Dare Button
    @IBOutlet private var infoTextField:UITextField!
    @IBOutlet public var accountImageView:UIImageView!
    
    /// Sets SM account information text
    func setAccountInfo(info:String?, dareEnabled:Bool, publicEnabled:Bool){
        if info == nil{
            infoTextField.placeholder = "Please enter account detail"
        }
        infoTextField.text = info
        
        rightEntitlementButton.isTurnedOn = dareEnabled
        let rbi:UIImage? = (dareEnabled) ? #imageLiteral(resourceName: "yellow") : #imageLiteral(resourceName: "Empty Highlight");
        rightEntitlementButton.setImage(rbi, for: .normal)
        
        leftEntitlementButton.isTurnedOn = publicEnabled
        let lbi:UIImage? = (publicEnabled) ? #imageLiteral(resourceName: "yellow") : #imageLiteral(resourceName: "Empty Highlight");
        leftEntitlementButton.setImage(lbi, for: .normal)
    }
    
    func setDare(enabled:Bool){
        rightEntitlementButton.isTurnedOn = enabled
        let rbi:UIImage? = (enabled) ? #imageLiteral(resourceName: "yellow") : #imageLiteral(resourceName: "Empty Highlight");
        rightEntitlementButton.setImage(rbi, for: .normal)
    }
    
    func setPublic(enabled: Bool){
        
        leftEntitlementButton.isTurnedOn = enabled
        let lbi:UIImage? = (enabled) ? #imageLiteral(resourceName: "yellow") : #imageLiteral(resourceName: "Empty Highlight");
        leftEntitlementButton.setImage(lbi, for: .normal)
        
        // If public enabled - so is dare
        if enabled{
            setDare(enabled: true)
        }
    }
    
    /// Sets SM Image, and field name type
    func setAccountType(type:String){
        // Hunt for type to local icon in assets
        let imgNameBuilder = "icon \(type)"
        let smImage:UIImage? = UIImage(named: imgNameBuilder)
        if (smImage == nil){
            print("[public profile] image for social media: \(type) does not exist!")
        }
        accountImageView.image = smImage
        fieldName = type
    }
    
    func getFieldName() -> String?{
        return fieldName
    }
    
    func getAccountInfo() -> String?{
        return infoTextField.text
    }
    
    func disableEditing(){
        infoTextField.isEnabled = false
        infoTextField.borderStyle = .none
        
        accountImageView.isUserInteractionEnabled = false
        accountImageView.layer.borderWidth = 0
        accountImageView.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func enableEditing(){
        infoTextField.isEnabled = true
        infoTextField.borderStyle = .roundedRect
        
        accountImageView.isUserInteractionEnabled = true
        accountImageView.layer.borderWidth = 1.5
        accountImageView.layer.borderColor = UIColor.lightGray.cgColor
        accountImageView.layer.cornerRadius = 5
    }
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

//
//  ParseImageSource.swift
//  ImageSlideshow
//
//  Created by Jaime Agudo Lopez on 14/01/2017.
//
/// Input Source to image using Parse
public class ParseSource: NSObject, InputSource {
    var file: PFFile
    var placeholder: UIImage?
    
    /// Initializes a new source with URL and optionally a placeholder
    /// - parameter url: a url to be loaded
    /// - parameter placeholder: a placeholder used before image is loaded
    public init(file: PFFile, placeholder: UIImage? = nil) {
        self.file = file
        self.placeholder = placeholder
        super.init()
    }
    
    @objc public func load(to imageView: UIImageView, with callback: @escaping (UIImage) -> Void) {
        self.file.getDataInBackground {(data: Data?, error: Error?) in
            if let data = data, let image = UIImage(data: data) {
                imageView.image = image
                callback(image)
            }
        }
    }
}


