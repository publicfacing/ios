//
//  PublicProfilesSwipeVC.swift
//  Dare-iOS
//
//  Created by Utkarsh Kumar on 1/19/17.
//  Copyright © 2017 Dare. All rights reserved.
//

import UIKit
import EZSwipeController
import Parse

class PublicProfilesSwipeVC: EZSwipeController{

    @IBOutlet weak var titleLabel: UILabel!
    
    /// Pass in the list of people to be swippable
    public var people:[PFUser]!
    public var selectedPerson:PFUser!
    
    var activeIndex:Int!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        assert(people.count >= 1, "Must pass atlease one person to swipeable list")
        assert(selectedPerson != nil, "Must pass one active person to swipeable list")
        
        // Do any additional setup after loading the view.
        navigationController?.navigationBar.isHidden = false
        automaticallyAdjustsScrollViewInsets = false
        
        let fullName = self.selectedPerson.object(forKey: "name") as? String
        let words = fullName?.components(separatedBy: " ")
        titleLabel.text = words?.first
        
        activeIndex = people.index(of: selectedPerson)
    }
    
    override func setupView() {
        super.setupView();
        datasource = self
        self.navigationBarShouldNotExist = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("Memory Warning: Swipe VC");
    }
}

extension PublicProfilesSwipeVC: EZSwipeControllerDataSource{
    func viewControllerData() -> [UIViewController] {
        var viewControllers = [UIViewController]()
        let storyboard = self.storyboard!
        
        for anUser in self.people{
            // Create view controllers in swipe format for every user in list
            let storyid = PublicProfileTableViewController.storyboardId
            let vc = storyboard.instantiateViewController(withIdentifier: storyid) as! PublicProfileTableViewController
            vc.user = anUser
            let navBarHeight:CGFloat = navigationController?.navigationBar.frame.height ?? 0
            vc.navBarOffset = navBarHeight + UIApplication.shared.statusBarFrame.height
            
            viewControllers.append(vc)
        }
        
        return viewControllers
    }
    
    func indexOfStartingPage() -> Int {
        return self.people.index(of: selectedPerson) ?? 0
    }
    
    func changedToPageIndex(_ index: Int) {
        // Change Title 
        let fullName = people[index].object(forKey: "name") as? String
        let words = fullName?.components(separatedBy: " ")
        titleLabel.text = words?.first
    }
}

