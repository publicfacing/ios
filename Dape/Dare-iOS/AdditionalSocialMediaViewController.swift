//
//  AdditionalSocialMediaViewController.swift
//  Dape-iOS
//
//  Created by Utkarsh Kumar on 3/9/17.
//  Copyright © 2017 Epoke. All rights reserved.
//

import UIKit

protocol SocialMediaChosen {
    func didChooseSocialMedia(smFieldName:String)
}

class AdditionalSocialMediaViewController: UIViewController {
    public var availableSocialMediaFields:[String]?
    var delegate:SocialMediaChosen?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard  let available = availableSocialMediaFields else{
            print("warning: no sm fields passed in!")
            
            for index in 1...3{
                let imgView = view.viewWithTag(index) as? UIImageView
                imgView?.image = #imageLiteral(resourceName: "Empty Image")
            }
            
            return
        }
        
        
        // Set Sm Icons
        for (index, smField) in available.enumerated(){
            let tag = index + 1;
            guard let imageView = self.view.viewWithTag(tag) as? UIButton else{
                print("warning: cant set more sm items")
                continue
            }
            
            // DO stuff with SM
            let smImage = UIImage(named: "icon \(smField)")
            imageView.setImage(smImage, for: .normal)
        }
    }
    
    // MARK: - Button Actions
    @IBAction func didTapSocialMedia(_ sender: UIButton){
        let index = sender.tag - 1
        guard  let available = availableSocialMediaFields else{
            return;
        }

        if let smField = available[safe: index]{
            delegate?.didChooseSocialMedia(smFieldName: smField)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
