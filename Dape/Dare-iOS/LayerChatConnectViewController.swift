//
//  LayerChatConnectViewController.swift
//  Dare-iOS
//
//  Created by Utkarsh Kumar on 2/18/17.
//  Copyright © 2017 Epoke. All rights reserved.
//

import UIKit
import Parse
import LayerKit
import Atlas
import SystemConfiguration

private let SegueEnterApp = "segue_enter_app"
class LayerChatConnectViewController: UIViewController{
    public var shouldSelectHomeTab:HomeViewControllerTabs?
    
    private var layerClient:LYRClient!
    private var currentUser:PFUser!
    //MARK: Storyboard outlets
    @IBOutlet weak var activity:UIActivityIndicatorView!
    @IBOutlet weak var statusLabel:UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.default
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        activity.hidesWhenStopped = true
        
        assert(PFUser.current() != nil)
        assert(PFUser.current()?.objectId != nil,
               "current user must be registered on db")
        currentUser = PFUser.current()!
        
        /*
        let deepQuery:PFQuery = PFUser.query()!
        deepQuery.whereKey("objectId", equalTo: currentUser.objectId!)
        deepQuery.includeKeys(["checkedIn"])
        */
        
        // Refrence APP delegate's Layer Client
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // Layer Setup
        self.layerClient = appDelegate.layerClient
        assert(appDelegate.layerClientIsInitialized)
        
        let imgTypes:Set<String> = [ATLMIMETypeImagePNG, ATLMIMETypeImageJPEG, ATLMIMETypeImageJPEGPreview, ATLMIMETypeImageGIF, ATLMIMETypeImageGIFPreview, ATLMIMETypeLocation]
        layerClient.autodownloadMIMETypes = imgTypes
        layerClient.autodownloadMaximumContentSize = 1024 * 1000;
        layerClient.diskCapacity = LYRSize(1024 * 1024 * 100); //Sets the limit to 50 MB

        
        assert(layerClient != nil,
               "LYR Client must be init at this point")
        assert(appDelegate.layerClientIsInitialized == true,
               "LYR Client must be init at this point")
        assert(appDelegate.layerClient != nil,
               "Must be set at this point")
        
        setStatusIcon(image: #imageLiteral(resourceName: "cloud"))
        setMessageOnUI(message: "Refreshing user data...",
                       startIndicator: true)
        
        // Sync refresh user
        currentUser.fetchInBackground(block: { (result, error) in
            guard let fetched = result as? PFUser, error == nil else{
                self.setErrorOnUI(error: error,
                             alternativeMessage: "Error connecting to server")
                self.showAlertOnRootViewController(title: "Error",
                                              error: error,
                                              alternativeMessage: "Error refreshing user data")
                return;
            }
            
            // Fetch was successful
            self.currentUser = fetched
            // DO Layer Stuff Here
            self.setStatusIcon(image: #imageLiteral(resourceName: "message_received"))
            self.setMessageOnUI(message: "Loading conversations ...")
            self.connnectLayerClient(client: self.layerClient, currentUserID: fetched.objectId!)
        })
    }
    
    //MARK:- NEW Layer Auth
    /// Start of all layer related authentication procedure
    func connnectLayerClient(client:LYRClient, currentUserID userID:String) -> Void
    {
        guard !layerClient.isConnected else{
            print("[layer connect] checking for auth user ..")
            checkForAuthUser(layerClient: client, currentUserId: userID)
            return
        }
        
        if layerClient.isConnecting{
            print("[layer connect] waiting to fishing connection ..")
            // Connection in progress
            while !layerClient.isConnected{
                // Stall while connecting
            }
            
            print("[layer connect] checking for auth user ..")
            checkForAuthUser(layerClient: client, currentUserId: userID)
        }
        else{
            layerClient.connect{ (connected, error) in
                if !connected{
                    let errormessage = "Failed to connect to layer services.\nCheck your network connection."
                    self.setErrorOnUI(error: error,
                                      alternativeMessage: errormessage)
                    
                    self.alertWith(error: error,
                                   title: "Conversation connection error",
                                   alternativeMessage: errormessage,
                                   handler: { (action) in
                                    self.segueToLoginScreen()})
                }
                else{
                    print("[layer connect] checking for auth user ..")
                    self.checkForAuthUser(layerClient: client, currentUserId: userID)
                }
            }
        }
    }
    
    private func checkForAuthUser(layerClient client:LYRClient, currentUserId:String)
    {
        assert(client.isConnected)
        guard let authUser = client.authenticatedUser else{
            self.requestNonce(client: client, userID: currentUserId, compeletion: { (finshed, error, alt) in
                guard finshed, error == nil else{
                    self.setErrorOnUI(error: error,
                                      alternativeMessage: alt ?? "no description")
                    
                    self.alertWith(error: error,
                                   title: "Conversation connection error",
                                   alternativeMessage: "no description",
                                   handler: { (action) in
                                    self.segueToLoginScreen()})
                    
                    return
                }
                
                // Fully authenticated
                DispatchQueue.main.async {
                    self.finishLoadingOnUI(message: "Success")
                    self.segueToEnterApp()
                }
            })
            
            return
        }
        
        // a user already authenticated
        let userid = authUser.userID
        print("[layer-client]: >layer already authenticated as user \(userid)")
        
        // If the layerClient is authenticated with the requested currentUserId, complete the authentication process.
        if authUser.userID == currentUserId
        {
            // Done with Authentication
            DispatchQueue.main.async {
                self.finishLoadingOnUI(message: "Success!")
                self.segueToEnterApp()
            }
        }
        else{
            print("[layer-client]: >layer de-auth...")
            //If the authenticated userID is different, then deauthenticate the current client and re-authenticate with the new userID.
            layerClient.deauthenticate(completion: { (success, error) -> Void in
                if error == nil{
                    self.requestNonce(client: client, userID: currentUserId, compeletion: { (finshed, error, alt) in
                        guard finshed, error == nil else{
                            self.setErrorOnUI(error: error,
                                              alternativeMessage: alt ?? "no description")
                            
                            self.alertWith(error: error,
                                           title: "Conversation connection error",
                                           alternativeMessage: "no description",
                                           handler: { (action) in
                                            self.segueToLoginScreen()})
                            return
                        }
                        
                        // Fully authenticated
                        DispatchQueue.main.async {
                            self.finishLoadingOnUI(message: "Success")
                            self.segueToEnterApp()
                        }
                    })
                }
                else{
                    // Deauth failed
                    self.setErrorOnUI(error: error,
                                      alternativeMessage: "Layer client is not deauthenticating logged in user");
                    self.alertWith(error: error,
                                   title: "Conversation connection error",
                                   alternativeMessage: "no description",
                                   handler: { (action) in
                                    self.segueToLoginScreen()})
                }
            })
        }
    }
    
    /// Request Layer Nonce - returns: (success, error, alternative message)
    func requestNonce(client:LYRClient, userID:String, compeletion:@escaping(Bool, Error?, String?) -> Void)
    {
        /*
         * 1. Request nonce from Layer.  Each nonce is valid for 10 minutes after
         *    creation, after which you will have to generate a new one.
         */
        client.requestAuthenticationNonce { (nonce:String?, error:Error?) in
            guard let nonceToken = nonce, error == nil else{
                
                compeletion(false, error, "Nonce token from Layer does not exist")
                return
            }
            
            if nonceToken.isEmpty{
                
                compeletion(false, error, "Nonce token from Layer is empty")
                return
            }
            
            /*
             * 2. Connect to your backend to generate an identity token.
             */
            print("☁️️ [layer-client]: requesting cloud generated token")
            PFCloud.callFunction(inBackground: "generateToken",
                                 withParameters: ["nonce": nonceToken, "userID": userID],
                                 block: { (object:Any?, error:Error?) in
                                    
                                    
                guard let identityToken = object as? String, error == nil else{
                    let errmsg = "Dape servers are not issuing valid identity token at this time"
                    compeletion(false, error, errmsg)
                    return;
                }
                
                /*
                 * 3. Submit identity token to Layer for validation
                 */
                self.layerClient.authenticate(withIdentityToken: identityToken, completion: { (authenticatedUser, lyrerr) in
                    if let authUser = authenticatedUser {
                        print("[layer-client]: authenticated as user \(authUser.userID)")
                        // Done with Authentication
                        compeletion(true, nil, nil);
                    }
                    else {  // Step 3 Error
                        compeletion(false, lyrerr, "Layer could not verify id token")
                    }
                });
            })
        }
    }
    
    // MARK: - UI
    func setErrorOnUI(error:Error?, alternativeMessage:String = "no description") -> Void
    {
        var errorMessage = error?.localizedDescription ?? alternativeMessage
        if let error = error{
            if (error as NSError).code == 209{
                errorMessage = "Your login session has expired. Please login again."
            }
        }
        
        print("[layer connect] error: \(errorMessage)")
        self.statusLabel.text = errorMessage
        self.activity.stopAnimating()
    }
    
    func setMessageOnUI(message:String, startIndicator:Bool = false) -> Void
    {
        self.statusLabel.text = message
        if startIndicator{
            self.activity.startAnimating()
        }
    }
    
    func finishLoadingOnUI(message:String)
    {
        self.activity.stopAnimating()
        self.statusLabel.text = message
    }
    
    func setStatusIcon(image img:UIImage)
    {
        self.statusImage.image = img
    }
    
    // MARK: - Segue
    func segueToLoginScreen(){
        let storyboardid = LoginViewController.storyboardId
        let loginvc = self.storyboard!.instantiateViewController(withIdentifier: storyboardid)
        present(loginvc, animated: true, completion: nil)
    }
    
    func segueToEnterApp(){
        self.performSegue(withIdentifier: SegueEnterApp, sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueEnterApp{
            // pass our intializers
            guard let dvcActiveTab = shouldSelectHomeTab else{
                return
            }
            let dvc = segue.destination as! MainTabBarController
            dvc.defaultSelectedTab = dvcActiveTab
        }
    }
    
    // MARK: - Helpers
    func showAlertOnRootViewController(title:String, error:Error?, alternativeMessage:String = "uknown")
    {
        var displayMessage:String = error?.localizedDescription ?? alternativeMessage
        if let error = error{
            if (error as NSError).code == 209{
                // Invalid session Error
                displayMessage = "Your login session has expired. Please login again."
            }
        }
        
        let alert = UIAlertController(title: title,
                                      message: displayMessage,
                                      preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Login Again", style: .destructive) { (action) in
            self.segueToLoginScreen()
        }
        alert.addAction(action)
        
        // 5 Seconds Later
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            if let root = UIApplication.shared.keyWindow?.rootViewController{
                root.present(alert, animated: true, completion: nil)
            }
            else{
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
}

public class Reachability {
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
}

/*
 currentUser.fetchInBackground {(refreshed, err) in
 guard let fetched = refreshed as? PFUser, err == nil else{
 
 self.setErrorOnUI(error: err,
 alternativeMessage: "Error connecting to server")
 
 self.alertWith(error: err,
 title: "Failed to update user data",
 alternativeMessage: "Error refreshing user data",
 handler: { (action) in
 
 self.segueToLoginScreen()
 });
 
 return;
 }
 
 self.currentUser = fetched
 // DO Layer Stuff Here
 self.setStatusIcon(image: #imageLiteral(resourceName: "message"));
 self.setMessageOnUI(message: "Loading conversations ...")
 self.connnectLayerClient(client: self.layerClient, currentUserID: fetched.objectId!)
 }
 */
