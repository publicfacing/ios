//
//  ConversationListViewController.swift
//  Competish
//
//  Created by Utkarsh Kumar on 11/21/15.
//  Copyright © 2015 Kanpurwala. All rights reserved.
//
import UIKit
import Atlas
import Parse
/*
class ConversationListViewController: ATLConversationListViewController, ATLConversationListViewControllerDelegate, ATLConversationListViewControllerDataSource
{
    // Private Var
    var group: PFObject!    // Reference Var for queries and chat fixes
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.group = (self.tabBarController as? GroupTabBarController)?.group;
        
        print("ViewDidLoad: Conversation List View Controller", "Passed Group = ", group["name"] as? String)
        
        self.dataSource = self
        self.delegate = self
        
        let composeItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Compose, target: self, action: Selector("composeButtonTapped:"))
        self.navigationItem.setRightBarButtonItem(composeItem, animated: false)
        
        // Title View
        let titleViewFrame = CGRectMake(0, 0, 145, 30);        // Title View Frame = 145 x 30
        let titleView = UIView(frame: titleViewFrame);
        // Title Label Setup
        let titleLabel = UILabel(frame: titleViewFrame);
        titleLabel.font = UIFont.systemFontOfSize(22);
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.textAlignment = .Center;
        titleLabel.text = "Chat"
        
        titleView.backgroundColor = UIColor.clearColor();
        titleView.addSubview(titleLabel);
        
        self.navigationItem.titleView = titleView;
        
        setupUI()
        // Background Color
        view.backgroundColor = UIColor(white: 0.90, alpha: 1);
    }
    
    func setupUI()
    {
        // UI Setup
        ATLConversationTableViewCell.appearance().cellBackgroundColor = UIColor.clearColor()
        ATLConversationTableViewCell.appearance().conversationTitleLabelColor = Helper.defaultColor();
        ATLAvatarImageView.appearance().initialsColor = UIColor.whiteColor()
        ATLAvatarImageView.appearance().imageViewBackgroundColor = Helper.defaultColor()
    }
    
    // MARK - ATLConversationListViewControllerDelegate Methods
    func conversationListViewController(conversationListViewController: ATLConversationListViewController, didSelectConversation conversation:LYRConversation) {
        self.presentControllerWithConversation(conversation)
    }
    
    func conversationListViewController(conversationListViewController: ATLConversationListViewController, didDeleteConversation conversation: LYRConversation, deletionMode: LYRDeletionMode) {
        print("Conversation deleted")
    }
    
    func conversationListViewController(conversationListViewController: ATLConversationListViewController, didFailDeletingConversation conversation: LYRConversation, deletionMode: LYRDeletionMode, error: NSError?) {
        print("Failed to delete conversation with error: \(error)")
    }
    
    func conversationListViewController(conversationListViewController: ATLConversationListViewController, didSearchForText searchText: String, completion: ((Set<NSObject>!) -> Void)?) {

        UserManager.sharedManager.queryForUserInGroupWithName(self.group, searchText: searchText, completion: { (participants: NSArray?, error: NSError?) in
            if error == nil {
                if let callback = completion {
                    callback(NSSet(array: participants as! [AnyObject]) as Set<NSObject>)
                }
            } else {
                if let callback = completion {
                    callback(nil)
                }
                print("Error searching for Users by name: \(error)")
            }
        })
        
        //MARK: Logic Update Trace
        /*
        UserManager.sharedManager.queryForUserWithName(searchText) { (participants: NSArray?, error: NSError?) in
            if error == nil {
                if let callback = completion {
                    callback(NSSet(array: participants as! [AnyObject]) as Set<NSObject>)
                }
            } else {
                if let callback = completion {
                    callback(nil)
                }
                print("Error searching for Users by name: \(error)")
            }
        }
        */
    }
    
    func conversationListViewController(conversationListViewController: ATLConversationListViewController!, avatarItemForConversation conversation: LYRConversation!) -> ATLAvatarItem! {
        if conversation.lastMessage == nil {
            return nil
        }
        let userID: String = conversation.lastMessage.sender.userID
        if userID == PFUser.currentUser()!.objectId {
            return PFUser.currentUser()
        }
        let user: PFUser? = UserManager.sharedManager.cachedUserForUserID(userID)
        if user == nil {
            UserManager.sharedManager.queryAndCacheUsersWithIDs([userID], completion: { (participants, error) in
                if participants != nil && error == nil {
                    self.reloadCellForConversation(conversation)
                } else {
                    print("Error querying for users: \(error)")
                }
            })
        }
        return user;
    }
    
    // MARK - ATLConversationListViewControllerDataSource Methods
    func conversationListViewController(conversationListViewController: ATLConversationListViewController, titleForConversation conversation: LYRConversation) -> String {
        if conversation.metadata["title"] != nil {
            return conversation.metadata["title"] as! String
        } else {
            let listOfParticipant = Array(conversation.participants)
            let unresolvedParticipants: NSArray = UserManager.sharedManager.unCachedUserIDsFromParticipants(listOfParticipant)
            let resolvedNames: NSArray = UserManager.sharedManager.resolvedNamesFromParticipants(listOfParticipant)
            
            if (unresolvedParticipants.count > 0) {
                UserManager.sharedManager.queryAndCacheUsersWithIDs(unresolvedParticipants as! [String]) { (participants: NSArray?, error: NSError?) in
                    if (error == nil) {
                        if (participants?.count > 0) {
                            self.reloadCellForConversation(conversation)
                        }
                    } else {
                        print("Error querying for Users: \(error)")
                    }
                }
            }
            if (resolvedNames.count > 0 && unresolvedParticipants.count > 0) {
                let resolved = resolvedNames.componentsJoinedByString(", ")
                return "\(resolved) and \(unresolvedParticipants.count) others"
            } else if (resolvedNames.count > 0 && unresolvedParticipants.count == 0) {
                return resolvedNames.componentsJoinedByString(", ")
            } else {
                return "Conversation with \(conversation.participants.count) users..."
            }
        }
    }
    
    
    // MARK:- Conversation Selection From Push Notification
    func presentConversation(conversation: LYRConversation) {
        self.presentControllerWithConversation(conversation)
    }
    
    // MARK:- Conversation Selection
    
    // The following method handles presenting the correct `ConversationViewController`, regardeless of the current state of the navigation stack.
    func presentControllerWithConversation(conversation: LYRConversation)
    {
        let shouldShowAddressBar: Bool  = conversation.participants.count > 2 || conversation.participants.count == 0
        let conversationViewController: ConversationViewController = ConversationViewController(layerClient: self.layerClient)
        conversationViewController.displaysAddressBar = shouldShowAddressBar
        conversationViewController.conversation = conversation
        conversationViewController.group = self.group
        
        if self.navigationController!.topViewController == self {
            self.navigationController!.pushViewController(conversationViewController, animated: true)
        } else {
            var viewControllers = self.navigationController!.viewControllers
            let listViewControllerIndex: Int = self.navigationController!.viewControllers.indexOf(self)!
            viewControllers[listViewControllerIndex + 1 ..< viewControllers.count] = [conversationViewController]
            self.navigationController!.setViewControllers(viewControllers, animated: true)
        }
    }
    
    
    // MARK - Actions
    func composeButtonTapped(sender: AnyObject) {
        let controller = ConversationViewController(layerClient: self.layerClient)
        controller.group = self.group
        
        controller.displaysAddressBar = true
        self.navigationController!.pushViewController(controller, animated: true)
    }
}
 */
