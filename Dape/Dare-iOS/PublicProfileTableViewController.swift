//
//  PublicProfileTableViewController.swift
//  Dare-iOS
//
//  Created by Utkarsh Kumar on 1/19/17.
//  Copyright © 2017 Dare. All rights reserved.
//

import UIKit
import Parse
import ParseUI
import ImageSlideshow
import LayerKit
import KDLoadingView
import FBSDKCoreKit

class PublicProfileTableViewController: UITableViewController {
    static let storyboardId = "storyboardId_public_profile"
    
    /// Foces TabBar to Hide if toggled
    public var forceHideTabBar:Bool = false
    
    /// Converstaion with User ** NOT Current User
    public var user:PFUser!
    public var navBarOffset:CGFloat = 0
    private var currentUser:PFUser!
    private var operationInProgress:Bool = false
    
    /** Allows you to go back to destination instead of
        pushing new Messaging Interface
    */
    public var dontStartNewConversations:Bool = false
    
    /** Place where the user is at. NOT the user's current place
     */
    private var place:PFObject?
    
    // MARK: Storyboard Outlets
    @IBOutlet weak var profileImageView:PFImageView!
    @IBOutlet weak var picDots: UIImageView!

    @IBOutlet weak var profileAboutLabel:UILabel!
    //@IBOutlet weak var backgroundImage:UIImageView!
    @IBOutlet weak var conversationButton: UIButton!
    @IBOutlet weak var dareButton: UIButton!
    
    @IBOutlet weak var mututalFriendsLabel: UILabel!
    
    // Social Media Labels
    @IBOutlet weak var socailMediaCell1:StaticSocialMediaCell!
    @IBOutlet weak var socailMediaCell2:StaticSocialMediaCell!
    @IBOutlet weak var socailMediaCell3:StaticSocialMediaCell!
    @IBOutlet weak var socailMediaCell4:StaticSocialMediaCell!
    @IBOutlet weak var socailMediaCell5:StaticSocialMediaCell!
    private var allCells:[StaticSocialMediaCell]!
    
    private let kUserSocialMediaList:[String] = [kUserSocialMedia1, kUserSocialMedia2,
                                                 kUserSocialMedia3, kUserSocialMedia4,
                                                 kUserSocialMedia5]
    
    private var fullScreen:ImageSlideshow!
    private var layerClient:LYRClient!
    
    /// KDLoading Indicator displayed on top of all views in
    /// center screen
    private var loadingIndicator:KDLoadingView!
    
    //MARK: - Life Cycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if forceHideTabBar{
            tabBarController?.tabBar.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if forceHideTabBar{
            tabBarController?.tabBar.isHidden = false
        }
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        assert(user != nil,
               "Set user before segue to Public Profile Table View Controller")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        layerClient = appDelegate.layerClient
        assert(layerClient != nil, "LYR Client must be init at this point")
        assert(layerClient.isConnected, "LYR Client must be connected")
        assert(appDelegate.layerClientIsInitialized == true, "LYR Client must be init at this point")
        
        // Init my data vars
        currentUser = PFUser.current()!
        place = user["checkedIn"] as? PFObject
        
        automaticallyAdjustsScrollViewInsets = false
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0)
        
        // Setup
        //profileImageView.layer.cornerRadius = profileImageView.frame.width / 2
        //profileImageView.layer.masksToBounds = true
        tableView.contentInset = UIEdgeInsets(top: navBarOffset, left: 0, bottom: 0, right: 0)
        let centerFrame = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60));
        centerFrame.center = CGPoint(x: UIScreen.main.bounds.size.width*0.5,
                                     y: UIScreen.main.bounds.size.height*0.5)
        
        loadingIndicator = KDLoadingView(frame: centerFrame.frame,
                                         lineWidth: 3.0,
                                         firstColor: self.view.tintColor,
                                         secondColor: UIColor.darkGray,
                                         thirdColor: nil,
                                         duration: 1.0)
        loadingIndicator.hidesWhenStopped = true
        view.addSubview(loadingIndicator)
        
        mututalFriendsLabel.text = "You have __ mutual friends"
        
        // Do any additional setup after loading the view.
        assert(self.user != nil, "PFUser must be set to display this screen")
        
        profileImageView.file = user["profilePic"] as? PFFile
        profileImageView.loadInBackground()
        
        // Dots below picture
        displayPhotoDots(imageView: picDots)
        
        //profileNameLabel?.text = user["name"] as? String
        profileAboutLabel.text = user["about"] as? String
        allCells = [socailMediaCell1,socailMediaCell2,socailMediaCell3,socailMediaCell4,socailMediaCell5]
        
        // Load Social Media
        loadSocialMedia()
        
        // Full screen presentation
        profileImageView.isUserInteractionEnabled = true
        fullScreen = ImageSlideshow(frame: self.view.bounds)
        
        // Facebook Mutual Friends
        let fbkid = user["facebookID"] as! String
        getMututalFriendsCount(facebookID: fbkid) { (commonFriends, fbkerror) in
            if let errorString = fbkerror{
                print("FbkSDK Graph Error: ")
                print(errorString)
                
                self.mututalFriendsLabel.text = "Error: " + errorString
                return
            }
            // Print common friends
            print("found common friends: ", commonFriends)

            let displayMessage:String = commonFriends > 0 ? String(commonFriends) : "no"
            self.mututalFriendsLabel.text = "You have \(displayMessage) mutual friends."
        }
    }
    
    func displayPhotoDots(imageView:UIImageView){
        var picCount = 0
        for imgfield in ["leftPic","profilePic","rightPic"]{
            if user[imgfield] != nil{
                picCount += 1;
            }
        }
        if picCount == 1{
            imageView.image = #imageLiteral(resourceName: "one")
        }
        else if picCount == 2{
            imageView.image = #imageLiteral(resourceName: "two")
        }
        else {
            imageView.image = #imageLiteral(resourceName: "three")
        }
    }
    
    func loadSocialMedia(){
        var publicEntilements:[String]
        var temp:[String]?
        let empty = [String]()
        
        temp = user["publicEntitlements"] as? [String]
        publicEntilements = temp ?? empty
        
        for (index, entitlement) in publicEntilements.enumerated(){
            if index >= allCells.count{
                print("[public] Warning cannot have more than 5 sm in 'currentSocialMedia'")
                continue
            }
            
            let smKey = kUserSocialMediaList[index]
            let smInfo = user[smKey] as? String
            let smCell = allCells[index]
            smCell.setAccountInfo(info: smInfo)
            smCell.setAccountType(type: entitlement)
        }
        
        for cell in allCells{
            if cell.isActive() == false{
                cell.hideCell()
            }
        }
    }
    
    // MARK: - Layer + Database Operations
    /// Creates a new LYRConversation between participants
    func createNewConversation(withUser:PFUser) throws -> LYRConversation
    {
        assert(withUser.objectId != nil,
               "The target user does not have id on database yet")
        assert(Thread.isMainThread == false,
               "Dont do long operations on main thread")
        
        //let myId = self.currentUser.objectId!
        let participants:Set<String> = [withUser.objectId!]
        let options = LYRConversationOptions()
        options.distinctByParticipants = false
        
        let newConvo = try self.layerClient.newConversation(withParticipants: participants,
                                                            options: options)
        print("new convo created with id:",
              "\(newConvo.identifier) \n with",
              "\(newConvo.participants.count) users")
        
        return newConvo
    }
    
    /// Loads a New Conversation for Given User Activity
    func fetchExisting(conversation:PFObject) throws -> LYRConversation?
    {
        let type = conversation["type"] as! String
        assert(type == "conversation", "Activity not of type conversation")
        assert(Thread.isMainThread == false, "cannot do this operation on main thread")
        
        let convoIdentifier = conversation["cid"] as! String
        
        // Query for the group conversation
        let conversationQuery = LYRQuery(queryableClass:LYRConversation.self)
        conversationQuery.limit = 1
        conversationQuery.offset = 0
        
        // Constrain to group chat stored on parse
        conversationQuery.predicate = LYRPredicate(property: "identifier",
                                                   predicateOperator: .isEqualTo,
                                                   value: convoIdentifier);
        // Query for our conversation
        let conversations = try self.layerClient.execute(conversationQuery)
        var participantIDs = [String]()
        var toUser:PFUser!                  // Figure out the to user:
        let participants = conversation["participants"] as! [PFUser]
        
        for usr in participants{
            participantIDs.append(user.objectId!)
            if usr != self.currentUser{
                toUser = usr;
            }
        }
        assert(toUser != nil, "Receiver of the conversation is not present")
        
        guard let foundConversation = conversations.firstObject as? LYRConversation else{
            print("[flow] error Conversation is not found on layer servers")
            return nil;
        }
        let _ = try UserManager.sharedManager.queryAndCache(userIDs: participantIDs)
        return foundConversation
    }
    
    func getExistingConversation(forParticipants usrs:[PFUser]) -> (PFObject?, Error?)
    {
        let query = PFQuery(className: "UserActivity")
        query.whereKey("participants",
                       containsAllObjectsIn: usrs)
        query.includeKeys(["createdBy",
                           "participants",
                           "place"])
        query.order(byDescending: "when")   // Descending by date
        query.whereKey("type", equalTo: "conversation")
        
        // 72 Hour cutoff limit
        let now = Date()
        let cal = Calendar.current
        if let cutoffDate = cal.date(byAdding: .day, value: kQueryCutoffDayLimit, to: now){
            // Activities greater than 72 hours ago
            query.whereKey("when", greaterThanOrEqualTo: cutoffDate)
        }
        else{
            print(#function, "cutoff date not specified. Manually adding time cutoff")
            // seconds in 3 days
            let cutoffDate = now.addingTimeInterval(kQueryCutoffSecondsLimit)
            query.whereKey("when", greaterThanOrEqualTo: cutoffDate)
        }
        
        do{
            let found = try query.getFirstObject()
            return (found, nil)
        }
        catch{
            return (nil, error)
        }
        
    }
    
    func getMututalFriendsCount(facebookID:String, completion:@escaping(Int,String?) -> Void){
        /*
         /{user-id}?fields=context.fields%28all_mutual_friends%29&access_token={other-user-access-token}
        */
        let params = ["fields": "context.fields(mutual_friends)"]
        guard let request = FBSDKGraphRequest(graphPath: "/\(facebookID)", parameters: params, httpMethod: "GET") else{
            
            completion(-1, "could not intialize facebook graph request.")
            return
        }
        
        request.start(completionHandler: { (connection, data, error) in
            guard data != nil, error == nil else{
                let errorString = error?.localizedDescription ?? "facebook request could not be completed"
                
                print("FBSDK error: ", errorString)
                completion(-1, errorString)
                return
            }
            let response = data as? NSDictionary
            guard let context = (response?["context"] as? NSDictionary)?["id"] as? String else{
                
                completion(-1, "could not get facebook context for this user")
                return
            }
            
            let params = ["fields": ""]
            guard let graphRequest = FBSDKGraphRequest(graphPath: context, parameters: params) else{
                
                completion(-1, "could not intialize facebook graph request.")
                return
            }
            
            graphRequest.start(completionHandler: { (con, result, error) in
                if (error == nil){
                    //everything works print the user data
                    let data = result as? NSDictionary
                    let mutualFriends = (data?["mutual_friends"] as? NSDictionary)?["summary"] as? NSDictionary
                    
                    /*
                    if mutualFriends == nil{
                        completion(0, "mutual friends not found")
                        return
                    }
                     print("mutual friends ", mutualFriends ?? [])
                    */
                    let totalCount:Int = mutualFriends?["total_count"] as? Int ?? 0
                    completion(totalCount, nil)
                }
                else {
                    
                    let facebookError = error?.localizedDescription ?? "Facebook context graph request error"
                    print(facebookError)
                    completion(-1, facebookError)
                }
            })
        })
    }

    //MARK: - Button Actions
    @IBAction private func tappedProfileImage(_ sender: UITapGestureRecognizer) {
        // Full Screeen Load Image
        guard let _ = user["profilePic"] as? PFFile else{
            print("[public profile tv] warning: user does not have profile pic")
            self.alertForError(error: nil,
                               title: "Error",
                               alternativeMessage: "User is missing main profile picture")
            return
        }
        
        /// Add users pictures to carousel
        var availableImages = [ParseSource]()
        for imgfield in ["leftPic","profilePic","rightPic"]{
            if let limg = user[imgfield] as? PFFile{
                let psrc = ParseSource(file: limg, placeholder: #imageLiteral(resourceName: "Empty Image"))
                availableImages.append(psrc)
            }
        }
        fullScreen.setImageInputs(availableImages)
        fullScreen.setCurrentPage(1, animated:true)
        fullScreen.presentFullScreenController(from: self)
    }
    
    @IBAction private func tappedChat(_ sender: UIButton)
    {
        if dontStartNewConversations{
            // Just the user back - prob came from a Messaging interface
            if self.navigationController == nil{
                print("warning: action not as desired bc nvc is not present")
            }
            let _ = self.navigationController?.popViewController(animated: true)
            return
        }
        
        // Disable Button for operation in progress
        conversationButton.isEnabled = false
        
        // Begin UI Updates
        loadingIndicator.startAnimating()
        DispatchQueue.global(qos: .background).async{
            print("[UP] looking up activities ..")
            do {
                // Check for existing conversation
                let (found, _) = self.getExistingConversation(forParticipants: [self.currentUser, self.user])
                var conversation:LYRConversation, isNew:Bool
                
                if let conversationActivity = found{
                    // Fetch existing convo
                    print("[UP] found existing convo...",
                          "\n[UP] loading existing convo ...")
                    assert(conversationActivity["type"] as! String == "conversation")
                    
                    let convo = try self.fetchExisting(conversation: conversationActivity)
                    guard let fetchedConvo = convo else{
                        DispatchQueue.main.async {
                            print("[UP] fetching convo failed since unexpected nil in chaining")
                            self.loadingIndicator.stopAnimating();  // END UI Updates
                            self.alertForError(error: nil,
                                               title: "Error",
                                               alternativeMessage: "Specific conversation could not be fetched");
                        }
                        return
                    }
                    
                    print("[UP] found existing convo")
                    conversation = fetchedConvo
                    isNew = false
                }
                else{
                    // Create new conversation
                    print("[UP] creating new convo...")
                    conversation = try self.createNewConversation(withUser: self.user)
                    isNew = true
                }
                
                print("[UP] presenting conversation view controller ...")
                let convoVC = ConversationViewController(layerClient: self.layerClient,
                                                         layerconverstaion: conversation,
                                                         participants: [self.user.objectId! : self.user],
                                                         newConversation: isNew)
                DispatchQueue.main.async {
                    self.loadingIndicator.stopAnimating();  // END UI Updates
                    self.conversationButton.isEnabled = true
                    self.navigationController!.pushViewController(convoVC, animated: true)
                }
            }
            catch{
                DispatchQueue.main.async {
                    self.loadingIndicator.stopAnimating();  // END UI Updates
                    self.conversationButton.isEnabled = true
                    self.alertForError(error: error,
                                       title:"Error loading conversation")
                }
            }
        }
    }
    
    @IBAction private func tappedDare(_ sender: UIButton){
        guard let userPlaceId = self.place?.objectId else{
            self.alert(message: "You must be checked in to Dape", title: "Not Yet")
            return
        }
        let params = ["to": user.objectId!, "place": userPlaceId]
        
        // Begin UI Updates
        loadingIndicator.startAnimating()
        PFCloud.callFunction(inBackground: "dare", withParameters: params) { (recv, error) in
            self.loadingIndicator.stopAnimating();  // END UI Updates
            guard let data = recv as? String, error == nil else{
                self.alertForError(error: error,
                                   title: "Failed to Dape",
                                   alternativeMessage: "Dape could not be sent")
                return
            }
            
            self.alert(message: "\(data)", title: "Dape Sent")
            self.switchToFlowTab()
        }
    }
    
    /// Instruct the Tab Bar Controller to go to Flow Tab
    func switchToFlowTab(){
        let tabCount = tabBarController?.viewControllers?.count ?? 0
        if kFlowTabIndex >= tabCount{
            print("Warning: Operation can crash app. Index of tab controller is mismatched")
            return;
        }
        
        tabBarController?.selectedIndex = kFlowTabIndex
    }
}

//MARK: - Subclassing
class StaticSocialMediaCell: UITableViewCell, UITextFieldDelegate {
    /// Label for account info - "SM Account Name"
    @IBOutlet private var accountInfoTextView:UITextView!
    @IBOutlet private var accountImageView:UIImageView?
    
    /// parse social media entitlement field
    private var fieldName:String?
    
    /// Decides if this cell is being used or not
    private var cellIsActive:Bool = false
    
    func setAccountInfo(info:String?){
        cellIsActive = true
        accountInfoTextView.text = info ?? ""
    }
    
    func setAccountType(type:String){
        cellIsActive = true
        // Hunt for type to local icon in assets
        let imgNameBuilder = "icon \(type)"
        let smImage:UIImage? = UIImage(named: imgNameBuilder)
        if (smImage == nil){
            print("[public profile] image for social media: \(type) does not exist!")
        }
        accountImageView?.image = smImage
        fieldName = type
    }
    
    func isActive() -> Bool{
        return cellIsActive
    }
    
    func hideCell(){
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        //let width = contentView.frame.width
        //separatorInset
        self.separatorInset = UIEdgeInsetsMake(0, self.bounds.size.width, 0, 0);
    }
    
    // MARK: Text Field
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}

