//
//  MainTabBarController.swift
//  Dare-iOS
//
//  Created by Utkarsh Kumar on 12/21/16.
//  Copyright © 2016 Dare. All rights reserved.
//

import UIKit
import Parse

private let kNewUserMessage:String = "Please enter at least one Social Media and enable Public and Dare permissions";

enum HomeViewControllerTabs:Int {
    case places = 0
    case people = 1
    case flow = 2
    case profile = 3
    case settings = 4
}

class MainTabBarController: UITabBarController,UITabBarControllerDelegate{
    // Redirection interface
    public var defaultSelectedTab:HomeViewControllerTabs?
    
    private var tabBarIsEnabled:Bool = true
    private var userCheckInCount:Int = 0;
    private var dareCheckInCount:Int = 0;
    private var peopleTabBar:UITabBarItem?
    private var dareTabBar:UITabBarItem?
    
    private var currentUser:PFUser!
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // Unsuscribed to server push updates
        NotificationCenter.default.removeObserver(self,
                                                  name: AppDelegate.serverPushNotification,
                                                  object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Suscribed to server push updates
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(MainTabBarController.didRecieveRemoteNotification(fromServer:)),
                                               name: AppDelegate.serverPushNotification,
                                               object: nil)
    }
    
    override func viewDidLoad(){
        super.viewDidLoad()
        delegate = self
        currentUser = PFUser.current()!
        
        if let barItems = self.tabBar.items{
            peopleTabBar = barItems[safe: HomeViewControllerTabs.people.rawValue]
            dareTabBar = barItems[safe: HomeViewControllerTabs.flow.rawValue]
        }
        let warning:String = (peopleTabBar == nil) ? "[tab bar] warning: missing ref to people tab bar item" : ""
        print(warning)
        
        // Do any additional setup after loading the view.
        if currentUser.isNew{
            selectedIndex = HomeViewControllerTabs.profile.rawValue
            tabBarIsEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0){
                self.alert(message: kNewUserMessage,
                           title:"Info")
            }
        }
        else if let defaultTab = defaultSelectedTab{
            selectedIndex = defaultTab.rawValue
        }
    }
    
    //MARK: Tab Bar Actions
    func tabBar(isEnabled:Bool){
        tabBarIsEnabled = isEnabled
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return tabBarIsEnabled
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem){
        userCheckInCount = 0
        dareCheckInCount = 0
        item.badgeValue = nil;
    }
    
    //MARK: Notification Center Handlers
    @objc func didRecieveRemoteNotification(fromServer notification:Notification){
        let activityType:String = notification.userInfo?[AppDelegate.kParseActivityInfoKey] as? String ?? ""
        print("[tab bar] activity type: ", activityType)
        
        //Server push handler
        if activityType == ParsePushType.checkin.rawValue{
            userCheckInCount += 1;
            peopleTabBar?.badgeValue = String(userCheckInCount)
        }
        else if activityType == ParsePushType.dare.rawValue{
            dareCheckInCount += 1;
            dareTabBar?.badgeValue = String(dareCheckInCount)
        }
        else{
            print("[tab bar] activity type not supported by this controller")
        }
        
    }
}

extension Array {
    subscript (safe index: UInt) -> Element? {
        return Int(index) < count ? self[Int(index)] : nil
    }
}
