//
//  PlacesViewController.swift
//  Dare-iOS
//
//  Created by Utkarsh Kumar on 12/30/16.
//  Copyright © 2016 Dare. All rights reserved.

import UIKit
import GoogleMaps
import GooglePlaces
import Parse
import KDLoadingView

private let AnimationDuration:Double = 0.6
private let zoomOutLevel:Float = 10
private let zoomLevel: Float = 18.0

private let kCheckInDistanceLimit:Double = 150

private let minZoom:Float = 17
private let maxZoom:Float = 20
private let LIKELI_THRESHOLD:Double = 0.0025
private let AutoCheckoutThreshold:Double = 2700     // 45 Min
private let LocationUpdateLikelihoodThreshold:Double = 60 // in meters
let kFlowTabIndex:Int = 2

class PlacesViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var rbbi: UIBarButtonItem!
    @IBOutlet weak var lbbi: UIBarButtonItem!
    
    var currentUser:PFUser!
    var infoMarker:MyGoogleMarker! //This is the marker that shows up in the map
    
    var locationManager:CLLocationManager!
    var currentLocation: CLLocation?
    var placesClient: GMSPlacesClient!
    
    enum AutoCheckInError : Error {
        case BadLocation, NotCheckedIn, LocationNotDetermined, LimitExceeded
    }
    
    //MARK: UI Controls
    /// Table view refresh control
    var refreshControl: UIRefreshControl!
    /// Activity indicator located in middle of screen (used for table view)
    /// Note: This view is inserted below map view!
    //@IBOutlet weak var activityIndicator:UIActivityIndicatorView!
    /// Loading indicator for database activities
    @IBOutlet weak var loadingIndicator:KDLoadingView!
    
    // Local Cache Vars
    var likelihoodPlaces:[LocalPlace]?
    var mostLikelyPlace:LocalPlace?
    var selectedPlace:LocalPlace?
    
    // Locks
    var placesClientOperationInProgress = false
    //var autoRefreshList:Bool = true
    
    private let SupportedPlaceTypes = ["art_gallery", "bar", "cafe",
                                       "casino", "church", "gym",
                                       "library", "mosque", "museum",
                                       "night_club", "restaurant", "school",
                                       "synagogue", "university"]
    
    // MARK: - LifeCycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //autoRefreshList = true
        if mapView.isHidden == false{
            locationManager.startUpdatingLocation()
        }
        self.mapView.startRendering()
        autoCheckOut()
    }

    override func viewDidLoad(){
        super.viewDidLoad()
        
        // Setup Class Variables
        assert(PFUser.current() != nil)
        currentUser = PFUser.current()
        
        // Appearance
        automaticallyAdjustsScrollViewInsets = false
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(PlacesViewController.handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //tableView.contentInset = UIEdgeInsets(top: -26, left: 0, bottom: 0, right: 0)
        //autoRefreshList = true
        
        // Setup Class Variables
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters       // 10 M accuracy
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 10;                                        // 10 M Filter
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        
        placesClient = GMSPlacesClient.shared()
        
        // Google Map View Setup
        var adjHeight:CGFloat = view.frame.height
        if let tbc = self.tabBarController, let nvc = self.navigationController{
            adjHeight = view.frame.height - tbc.tabBar.frame.height - nvc.navigationBar.frame.height
        }
        else{
            print("[Places VC]: vc is **NOT** inside tbc and nvc as expected!\n")
        }
        
        infoMarker = MyGoogleMarker()
        var mapViewFrame:CGRect = view.frame
        mapViewFrame.size.height = adjHeight
        
        // Map View Setup Cont..
        mapView.frame = mapViewFrame
        mapView.center = view.center
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.mapType = .normal
        mapView.delegate = self
        mapView.isHidden = false         // Hiden until location is determined
    
        mapView.setMinZoom(minZoom, maxZoom: maxZoom)     // Min 15 - streets zoom max 20 - buildings
        
        /* Activity Indicator behind
        activityIndicator.hidesWhenStopped = true
        self.view.insertSubview(activityIndicator, belowSubview: mapView)
        self.view.sendSubview(toBack: self.tableView)
        */
        
        likelihoodPlaces = [LocalPlace]()
        
        // Update UI Operations
        if isUserCheckedIn(){
            self.updateUI(checkedIn: true)
        }
        else{
            self.updateUI(checkedIn: false)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //autoRefreshList = false
        
        locationManager.stopUpdatingLocation()
        self.mapView.stopRendering()
        //activityIndicator.stopAnimating()
    }
    
    func isUserCheckedIn() -> Bool{
        let place = self.currentUser["checkedIn"] as? PFObject
        return place != nil
    }

    // MARK:- UI Update Functions
    /// NOTE: Call with refreshed current user only!
    public func updateUI(checkedIn:Bool){
        if checkedIn{
            self.lbbi.title = nil
            self.lbbi.image = #imageLiteral(resourceName: "checkout")
            self.lbbi.isEnabled = true
            self.lbbi.tintColor = UIColor.red
        }
        else{
            self.lbbi.title = nil
            self.lbbi.image = nil
            self.lbbi.isEnabled = false
            self.lbbi.tintColor = UIColor.clear
        }
        
        tableView.reloadData()
    }
    
    // MARK: - Starter Functions
    func updateLikelihood(_ sender:UIRefreshControl?) -> Void{
        guard placesClientOperationInProgress == false else{
            print("[Places View Controller] warning operation already in progress")
            return
        }
        //print("[Places View Controller] updating places..")
        
        sender?.beginRefreshing()
        loadingIndicator.startAnimating()
        
        // Grab the lock
        placesClientOperationInProgress = true
        
        // Find new places nearby
        placesClient.currentPlace(callback: { (placeLikelihoods, error)->Void in
            // Release Lock
            self.placesClientOperationInProgress = false
            
            if let error = error
            {
                // End UI updates
                sender?.endRefreshing()
                self.loadingIndicator.stopAnimating()
                
                let altMessage = "error updating places from google places"
                self.alertForError(error: error,
                                   alternativeMessage: altMessage)
                //print(#function, "current place error: \(error.localizedDescription)")
                return;
            }
            
            // Clear old cache
            self.mapView.clear()
            self.likelihoodPlaces?.removeAll()
            self.tableView.reloadData()
            guard let likelihoodList = placeLikelihoods else{
                // End UI updates
                sender?.endRefreshing()
                self.loadingIndicator.stopAnimating()
                
                print(#function, "didnt receive a non nil list of likely places nearby")
                return
            }
            
            // Debug Print
            let len = likelihoodList.likelihoods.count
            //print("[Places View Controller] found \(len) places")
            
            //print("\nLikelihood Places:\n")
            for (index, likeliPlace) in likelihoodList.likelihoods.enumerated(){
                // Skip these places due to constaints
                if likeliPlace.likelihood <= LIKELI_THRESHOLD{
                    continue;
                }
                // Type Filtering
                if self.placeTypesIsVaid(types: likeliPlace.place.types) == false{
                    //print("non supported place type. skipping..")
                    continue
                }
                
                // Cache the list of places
                let local = LocalPlace(googlePlace: likeliPlace.place,
                                       likelihood: likeliPlace.likelihood)
                self.likelihoodPlaces!.append(local)
                
                let addressParts = likeliPlace.place.addressComponents
                
                let place:GMSPlace = likeliPlace.place
                
                // Add markers for nearby places.
                let marker = MyGoogleMarker(position: place.coordinate)
                marker.googlePlace = place;
                //GMSMarker(position: place.coordinate)
                
                if index == 0{
                    self.mostLikelyPlace = local
                }
                // Add to maps
                marker.title = place.name
                marker.snippet = place.formattedAddress
                marker.map = self.mapView
                
            }
            
            // End UI updates
            // Refresh Table View
            sender?.endRefreshing()
            self.loadingIndicator.stopAnimating()
            self.tableView.reloadData()
        })
    }
    
    func placeTypesIsVaid(types: [String]) -> Bool{
        for supported in SupportedPlaceTypes{
            if types.contains(supported){
                return true
            }
        }
        
        return false
    }
    
    /// Auto check out on UI
    func autoCheckOut() -> Void{
        weak var lbbiRef = self.lbbi
        weak var weakSelf = self
        
        // Handler for checking out the user
        let checkOutHandler = { (action:UIAlertAction) in
            self.checkout(confirmDialog: true)
        }
        
        let stayCheckedInHandler = { (action:UIAlertAction) in
            // Check for user's location, & self checkin
            do{
                try weakSelf?.renewCheckIn(completion: { (saved, error) in
                    guard saved, error == nil else{
                        print(#function, error?.localizedDescription ?? "n/a")
                        weakSelf?.alertForError(error: error, title: "Error")
                        return
                    }
                
                    // Everything went fine
                    print(#function, " success: checked in")
                })
            }
            catch AutoCheckInError.BadLocation{
                weakSelf?.alert(message: "location cannot be determined",
                                title: "Error")
                weakSelf?.checkout(confirmDialog: false)
            }
            catch AutoCheckInError.NotCheckedIn{
                weakSelf?.alert(message: "user is not checked into any place",
                                title: "Error")
                weakSelf?.checkout(confirmDialog: false)
            }
            catch AutoCheckInError.LocationNotDetermined{
                weakSelf?.alert(message: "place location could not be determined",
                                title: "Error")
                weakSelf?.checkout(confirmDialog: false)
            }
            catch AutoCheckInError.LimitExceeded{
                let message = "This place is no longer within the checkin distance limit"
                weakSelf?.alert(message: message,
                                title: "Oops")
                weakSelf?.checkout(confirmDialog: false)
            }
            catch{
                print(#function, error.localizedDescription)
            }
        }
        
        // Check for user info
        guard let lastCheckIn = currentUser["lastCheckInTime"] as? Date,
            let checkedInPlace = currentUser["checkedIn"] as? PFObject else{
                // Early exit
                print(#function,"user not checked in || last checkin time DNE")
                return;
        }
        
        checkedInPlace.fetchInBackground(block: { (place, error) in
            guard let checkedInPlace = place else{
                print("warning: user checked in place was not found")
                return
            }
        
            let timeDiff:Double = abs(lastCheckIn.timeIntervalSinceNow)
            
            print(#function, "last check in time: ", timeDiff.roundTo(places: 0),
                  " seconds")
            
            guard timeDiff > AutoCheckoutThreshold else {
                return;
            }
            
            // Ask to checkout the user
            let place_name = checkedInPlace["name"] as? String ?? "no name"
            let place_add = checkedInPlace["address"] as? String ?? "unknown address"
            let alert_message = "\(place_name)\n\(place_add)";
            self.alert(message: alert_message,
                       title: "Are you still there?",
                       actionTitle: "Check out",
                       successHandler: checkOutHandler,
                       cancelTitle: "I'm here",
                       cancelHandler: stayCheckedInHandler)
            
        })
        
        // End of Auto check out function
    }
    
    //MARK: - Location Delegate
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let newLocation = locations.last else{
            //print("[Places View Controller] error: no location in this update")
            return;
        }
        
        var deltaDistance:Double = Double.infinity;
        if let oldLocation = self.currentLocation{
            deltaDistance = newLocation.distance(from: oldLocation)
            /*
            print("[Places View Controller] - location update",
                  " delta ", deltaDistance.roundTo(places: 0))
             */
        }
        else{
            print("[Places View Controller] - new location update")
        }
        
        self.currentLocation = newLocation
        if deltaDistance > LocationUpdateLikelihoodThreshold{
            updateLikelihood(nil)
        }
        
        // Update Maps
        let camera = GMSCameraPosition.camera(withLatitude: newLocation.coordinate.latitude,
                                              longitude: newLocation.coordinate.longitude,
                                              zoom: zoomLevel)
        
        mapView.camera = camera
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
            case .restricted:
                self.alert(message: "Please give Dape full access to your location inside Settings>Privacy>Location Services.", title: "Location Error")
            
            case .denied:
                self.alert(message: "Please give Dape access to location services inside Settings>Privacy>Location Services.", title: "Location Error")
                // Display the map using the default location.
                showMapView()
            
            case .notDetermined:
                print("- Warning: Location status not determined.")
                //self.alert(message: "Location status not determined.", title: "Location Error")
            
            default:
                return;
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        
        if (error as NSError).code == 0{
            print("[places] simulator location ignoring error")
            return
        }
        
        alertForError(error: error, title: "Location Services Error")
        print("[places] Error: \(error)")
    }
    
    // MARK: - Button Actions
    @IBAction func toggleView(_ sender: UIBarButtonItem) {
        if !mapView.isHidden{
            // LIST VIEW State
            hideMapView();
            sender.image = #imageLiteral(resourceName: "Map small")
        }
        else{
            // MAP VIEW State
            showMapView()
            sender.image = #imageLiteral(resourceName: "Menu small")
        }
    }
    
    @IBAction func checkoutActionConfimation(_ sender: UIBarButtonItem?){
        
        let checkoutHandler = { (action:UIAlertAction) in
            self.checkout(confirmDialog: true)
        }
        
        alert(message: "Are you sure you want to check out from this place ?",
              title: "Confirm",
              actionTitle: "Checkout",
              successHandler: checkoutHandler,
              cancelTitle: "Cancel",
              cancelHandler: nil)
    }
    
    func hideMapView(){
        // Stop Updating Location
        //print("[Places View Controller] stopped location updates")
        locationManager.stopUpdatingLocation()
        
        // Hide with animation
        self.mapView.alpha = 1
        UIView.animate(withDuration: AnimationDuration, animations: {
            self.mapView.alpha = 0;
        }) { (finished) in
            self.mapView.isHidden = finished
        }
        
        //Show Map View Option
        self.rbbi.image = #imageLiteral(resourceName: "Map small");
    }
    
    func showMapView(){
        // Start location updates
        //print("[Places View Controller] began location updates")
        locationManager.startUpdatingLocation()
        
        self.mapView.alpha = 0;
        self.mapView.isHidden = false
        
        UIView.animate(withDuration: AnimationDuration) {
            self.mapView.alpha = 1;
        }
        
        //Show List view mode option
        self.rbbi.image = #imageLiteral(resourceName: "Menu small");
    }
    
    func switchToFlowTab(){
        let tabCount = tabBarController?.viewControllers?.count ?? 0
        if kFlowTabIndex >= tabCount{
            print("Warning: Operation can crash app. Index of tab controller is mismatched")
            return;
        }
        
        tabBarController?.selectedIndex = kFlowTabIndex
    }
    
    @objc func handleRefresh(refreshControl: UIRefreshControl) {
        //autoRefreshList = true
        updateLikelihood(refreshControl)
    }
}

//MARK: - Google Map View Delegate
extension PlacesViewController: GMSMapViewDelegate{
    func mapView(_ mapView: GMSMapView, didTapPOIWithPlaceID placeID: String, name: String, location: CLLocationCoordinate2D){
        infoMarker.snippet = "Loading..."
        infoMarker.position = location
        infoMarker.title = name
        infoMarker.opacity = 0;
        infoMarker.infoWindowAnchor.y = 1
        infoMarker.map = mapView
        mapView.selectedMarker = infoMarker
        
        // Lock selected place
        selectedPlace = nil
        
        print("Looking up place \(placeID) ...")
        // Query Selected Place
        placesClient.lookUpPlaceID(placeID, callback: { (gplace: GMSPlace?, error: Error?) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                self.alertForError(error: error)
                return
            }
            
            if let place = gplace{
                print("Place name \(place.name)")
                print("Place attributions \(place.attributions)")
                
                self.infoMarker.snippet = place.formattedAddress
                self.infoMarker.map = mapView
                self.infoMarker.googlePlace = place
                self.mapView.selectedMarker = self.infoMarker
                
                // Update class var
                self.selectedPlace = LocalPlace(googlePlace: place,
                                                likelihood: -1.0);
            }
            else {
                print("No place details for \(placeID)")
                self.alert(message: "No details found",
                           title: "Google Place Error")
            }
        })
        
    }
    
    /// loads images for a given place id - gmaps
    func loadFirstPhotoForPlace(localPlace: LocalPlace, cellPath:IndexPath) {
        assert(localPlace.fetchedImage == nil, "Cache already has image");
        assert(localPlace.fetchInProgress == false, "This image is already being fetched");
        assert(localPlace.fetchFinished == false, "This image has already been fetched");
        
        //print("Looking up photo for place \(localPlace.name) ...");
        UIApplication.shared.isNetworkActivityIndicatorVisible = true;
        
        // Set fetch in progress
        localPlace.fetchInProgress = true
        localPlace.fetchFinished = false
        
        placesClient.lookUpPhotos(forPlaceID: localPlace.getPlaceID()) { (photos, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("Error: \(error.localizedDescription)")
                localPlace.fetchInProgress = false;
                
            } else {
                if let firstPhoto = photos?.results.first {
                    localPlace.fetchInProgress = true
                    localPlace.fetchFinished = false

                    self.loadImageForMetadata(photoMetadata: firstPhoto, cellPath: cellPath, localPlace: localPlace)
                }
            }
        }
    }
    
    func loadImageForMetadata(photoMetadata: GMSPlacePhotoMetadata, cellPath:IndexPath, localPlace:LocalPlace) {
        
        //TODO: Save Memory and Data Usage:
        /*let maxcellHeight = self.tableView.frame.height / 3;
        let maxCellWidth = self.tableView.frame.width;
        let maxPhotoSize = CGSize(width: maxCellWidth, height: maxcellHeight)
        */
        placesClient.loadPlacePhoto(photoMetadata, callback: {
            (photo, error) -> Void in
            if let error = error {
                // TODO: handle the error.
                print("[Places Api] Error: \(error.localizedDescription)")
                
                // Update Local Data
                localPlace.fetchInProgress = false
                localPlace.fetchedImage = nil
                localPlace.fetchFinished = false
                
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                // Update Local Data
                localPlace.fetchInProgress = false
                localPlace.fetchedImage = photo
                localPlace.fetchFinished = true
                localPlace.fetchedAttributedText = photoMetadata.attributions
                
                // Reload Table View
                self.tableView.reloadRows(at: [cellPath], with: UITableViewRowAnimation.automatic)
            }
        })
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let myMarker = marker as? MyGoogleMarker
        if let place = myMarker?.googlePlace{
            selectedPlace = LocalPlace(googlePlace: place,
                                       likelihood: -1.0);
        }
        return false
    }

    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker){
        if selectedPlace == nil{
            alert(message: "Please wait for the place information to load then try again.",
                  title: "Info")
            return
        }
        else if isUserCheckedIn(){
            alert(message: "You are currently checked in at another place.",
                  title: "Info")
            return
        }
        
        // Check in the user to this place
        checkIn(place: self.selectedPlace!) { (success, error) in
            self.updateUI(checkedIn: success)
            // Lock selected place
            self.selectedPlace = nil
        }
    }
}

// MARK: - Database Operations
extension PlacesViewController{
    /// Checks in user to a given Google Place by calling the database cloud
    func checkIn(place: LocalPlace, completion:@escaping(Bool, Error?) -> Void){
        print("Check in to \(place.name)?")
        print("☁️️ Calling cloud...");
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        loadingIndicator.startAnimating()
        
        let placeTypes:String = place.getTypes().joined(separator: ",")
        
        let params:[String:Any] = ["place_id": place.placeID,
                                   "place_name": place.name,
                                   "place_address": place.getAddress(),
                                   "place_location_lat": place.getLocationCoordinate().latitude,
                                   "place_location_lon": place.getLocationCoordinate().longitude,
                                   "place_types": placeTypes,
                                   "user_id": currentUser.objectId!,
                                   "user_location_lat": currentLocation!.coordinate.latitude,
                                   "user_location_lon": currentLocation!.coordinate.longitude];
        
        PFCloud.callFunction(inBackground: "check-in", withParameters: params) { (result, err) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.loadingIndicator.stopAnimating()
            guard let data = result, err == nil else{
                print("[Place VC - Cloud] Error",
                      "\(err?.localizedDescription ?? "no description")")
                
                completion(false, err)
                
                // UI Alert
                self.alertForError(error: err, title: "☁️️ 'check-in' Error")
                return
            }

            print("[☁️️ places vc :check-in] recv data - \(data)")
            
            // Refresh user before returning
            self.currentUser.fetchInBackground(block: { (fetched, err) in
                completion(true, err)
                
                // UI Alert
                self.alert(message: "\(data)",
                            title: "You have just Checked in!")
                self.switchToFlowTab()
            })
        }
    }
    
    func checkout(confirmDialog:Bool){
        print("[☁️️ places vc] checking out user..")
        
        lbbi.image = nil;
        lbbi.title = "Checking out .."
        loadingIndicator.startAnimating()
        
        // Cloud checkout function
        PFCloud.callFunction(inBackground: "check-out", withParameters: nil) {(data, error) in
            self.loadingIndicator.stopAnimating()
            guard let str = data as? String, error == nil else{
                
                self.updateUI(checkedIn: true)
                
                if confirmDialog{
                    self.alertForError(error: error, title: "Check Out Error")
                }
                return
            }
            // Success -- refresh at this point
            
            print("[☁️️ check-out] cloud returned: ", str,
                  "refreshing user...")
            
            self.currentUser.fetchInBackground(block: { (fetched, err) in
                self.updateUI(checkedIn: false)
            })
        }
    }
    
    /// self checks in the user & updates his time stamp
    func renewCheckIn(completion:@escaping PFBooleanResultBlock) throws {
        
        guard let myLocation = currentLocation else{
            throw AutoCheckInError.BadLocation
        }
        guard let place = self.currentUser["checkedIn"] as? PFObject else{
            throw AutoCheckInError.NotCheckedIn
        }
        guard let placeGeoPoint = place["location"] as? PFGeoPoint else{
            throw AutoCheckInError.LocationNotDetermined
        }
        
        let dest:CLLocation = CLLocation(latitude: placeGeoPoint.latitude,
                                       longitude: placeGeoPoint.longitude)
        let distance = myLocation.distance(from: dest)
        if distance > kCheckInDistanceLimit{
            // exceeded check in dist limit
            throw AutoCheckInError.LimitExceeded
        }
        else{
            // self check-in logic
            self.currentUser["lastCheckInTime"] = Date()
            self.currentUser.saveInBackground(block: completion)
        }
    }
}


//MARK: - Table View Stuff
extension PlacesViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.likelihoodPlaces?.count ?? 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "place_cell", for: indexPath) as! PlaceCell
        let place = self.likelihoodPlaces![indexPath.row]
        let userIsCheckedIn:Bool = isUserCheckedIn()
        
        // Load cell with place details
        cell.placeTitle?.text = place.name
        cell.placeAddress?.text = place.getAddress()
        cell.placeImage.image = #imageLiteral(resourceName: "Empty Image")
        cell.placeDistance.text = nil
        
        // if user is not checked in
        // Every cell is available to check in
        cell.setCheckIn(availability: !userIsCheckedIn)
        
        if let currLoc = self.currentLocation{
            // Set location distances on cell
            let placeLocation = CLLocation(latitude: place.getLocationCoordinate().latitude,
                                           longitude: place.getLocationCoordinate().longitude)
            
            let distance:Double = placeLocation.distance(from: currLoc).roundTo(places: 0);
            cell.placeDistance.text = "\(distance) m"
        }
        
        // Google Place Images
        if let cacheImage = place.fetchedImage{
            // Cache is available
            cell.placeImage.image = cacheImage
        }
        else if place.fetchInProgress{
            // Do Nothing -- we will be called by the async loader when its done
        }
        else if place.fetchFinished{
            // No image exists for this place
            cell.placeImage.image = #imageLiteral(resourceName: "Empty Image")
        }
        else{
            // Async load
            loadFirstPhotoForPlace(localPlace: place, cellPath: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let chosenPlace:LocalPlace = self.likelihoodPlaces![indexPath.row];
        
        // Check in user - Place is available
        self.selectedPlace = chosenPlace
        self.checkIn(place: chosenPlace) { (success, error) in
            
            self.updateUI(checkedIn: success)
        }
    }
    
    // MARK: - Table View Appearance
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}

//MARK: - Subclasses
class PlaceCell: UITableViewCell {
    @IBOutlet var placeImage:UIImageView!
    @IBOutlet var placeTitle:UILabel!
    @IBOutlet var placeAddress:UILabel!
    @IBOutlet var placeDistance:UILabel!
    @IBOutlet var placeCheckInAvailableIcon:UIImageView!
    
    func setCheckIn(availability available:Bool){
        // Left align the distance label
        //(since there is icon)
        placeDistance.textAlignment = (available) ? NSTextAlignment.left : NSTextAlignment.right
        placeCheckInAvailableIcon.image = (available) ? #imageLiteral(resourceName: "Arrow") : nil;
        
        isUserInteractionEnabled = available
        selectionStyle = (available) ? UITableViewCellSelectionStyle.default : UITableViewCellSelectionStyle.none
    }
}

//TODO: Memory Leaks
class LocalPlace{
    public var fetchedImage:UIImage?
    public var fetchInProgress:Bool = false;
    public var fetchFinished:Bool = false;
    private var googlePlace:GMSPlace!
    public var fetchedAttributedText:NSAttributedString?
    public var likelihood:Double?
    
    var cordinate:CLLocationCoordinate2D{
        return self.googlePlace.coordinate
    }
    
    var name:String{
        return self.googlePlace.name
    }
    
    var placeID:String{
        return self.googlePlace.placeID
    }
    
    init(googlePlace gPlace: GMSPlace, likelihood prob:Double){
        self.googlePlace = gPlace;
        self.fetchedImage = nil
        self.fetchInProgress = false
        self.likelihood = prob
    }
    
    func getTypes() -> [String]{
        return self.googlePlace.types
    }
    
    func equals (compareTo:LocalPlace) -> Bool {
        return (self.googlePlace == compareTo.googlePlace);
    }
    
    func getAddress() -> String{
        guard let components = googlePlace.addressComponents else{
            return "not available"
        }
        
        let supported = ["street_number", "route", "locality", "postal_code"]
        
        var formattedAddress:String = ""
        for comp in components{
            if supported.contains(comp.type){
                formattedAddress += comp.name
                formattedAddress += " "
            }
        }

        return formattedAddress //?? "not available"
    }
    
    func getPlaceID()-> String{
        return googlePlace.placeID
    }
    
    func getLocationCoordinate() -> CLLocationCoordinate2D{
        return googlePlace.coordinate
    }
    
}

class MyGoogleMarker:GMSMarker{
    public weak var googlePlace:GMSPlace?
}

