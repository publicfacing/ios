//  Created by Utkarsh Kumar on 10/15/15.
//  Copyright © 2017 Utkarsh Kumar. All rights reserved.

import UIKit
import Parse
import FBSDKLoginKit
import FBSDKCoreKit
import ParseFacebookUtilsV4
import LayerKit
import SafariServices
//import ParseLiveQuery
private let LayerLoadingSegueId = "segue_load_layer"

class LoginViewController: UIViewController, FBSDKLoginButtonDelegate, UITextFieldDelegate,  UINavigationControllerDelegate{
    @IBOutlet weak var loginPlaceholder: UIView!
    @IBOutlet weak var loginStaticDescription: UITextView!
    @IBOutlet weak var loginButton: FBSDKLoginButton!
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var refreshControl: UIActivityIndicatorView!
    
    // Learn more 
    @IBOutlet weak var learnMoreLabel:UILabel!
    @IBOutlet weak var tosButton: UIButton!
    @IBOutlet weak var privacyButton: UIButton!
    @IBOutlet weak var privacyButton2: UIButton!
    
    @IBOutlet weak var loadingLabel: UILabel!
    
    // Global Var
    private var userIsNew:Bool = false
    private let fbsdkPermissions = ["public_profile", "email", "user_friends", "user_location", "user_birthday", "user_photos"];
    private let fbsdkUserFields:String = "name,location,email,id,gender,birthday,about,cover,hometown,website,relationship_status,link";
    
    private let fbsdkLoginError:String = "Login Failed. \nThe app is having trouble logging in with facebook.\nPlease check if the login information have not expired.\nAlternatively please check if the time on your phone is right as it may affect the authentication process.";
    static let storyboardId = "LoginViewController"
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.default
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
        
        // Login Button
        //loginButton = FBSDKLoginButton()
        loginButton.loginBehavior = .browser;
        loginButton.delegate = self;
        loginButton.layer.borderWidth = 0.4
        
        // Logo Adj
        logoImageView.layer.cornerRadius = logoImageView.frame.width / 2
        logoImageView.clipsToBounds = true
        loginButton.layer.cornerRadius = 5
        loginButton.clipsToBounds = true
        
        // Static Description Text
        loginStaticDescription.isEditable = false
        loginStaticDescription.isUserInteractionEnabled = false
        //companySlogan.text = "Welcome to Dare.\nYou are now a tap away from anyone."
        //companySlogan.textAlignment = .center
        
        // learn more button
        //let staticTextInfo = learnMoreLabel.attributedText!.string
        //learnMoreLabel.attributedText = attributedText
        
        // Get Fbk Permissions
        loginButton.readPermissions = fbsdkPermissions
        loginButton.backgroundColor = view.tintColor
        loginButton.setBackgroundImage(nil, for: .normal)
        
        // Constraints
        refreshControl.hidesWhenStopped = true
        
        // Check Current Login
        if userIsLoggedIn(){
            print("[Login] Access token or current user already exists!")
            // Logout this user from facebook && DB
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            FBSDKAccessToken.setCurrent(nil)
        }
        
    }
    
    func userIsLoggedIn() -> Bool{
        return FBSDKAccessToken.current() != nil || PFUser.current() != nil
    }
    
    @IBAction func privacyLinkTapped(_ sender:UIButton){
        performAction(forType: .privacy)
    }
    
    @IBAction func tosLinkTapped(_ sender:UIButton){
        performAction(forType: .tos)
    }
    
    func performAction(forType: RedirectType){
        // Redirects to url
        var endPoint:String = ""
        
        switch forType {
        case .tos:
            endPoint = "/termsofservice"
            break
        case .learn:
            endPoint = "/learnmore"
            break
        case .privacy:
            endPoint = "/privacy"
            break
        default:
            break
        }
        
        guard let url = URL(string: DapeURL + endPoint) else{
            alert(message: "could not perform this action", title: "Oops")
            return
        }
        
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
    
    
    // MARK: - Delegates
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        /*
        let token = result.token;
        let declined = result.declinedPermissions;
        let accepted = result.grantedPermissions;
        let cancelled = result.isCancelled;
        */
        
        let accessToken = FBSDKAccessToken.current()
        if result.isCancelled{
            print("[Login View Controller] fbsdk login cancelled ")
            return;
        }
        if let error = error{
            self.alertForError(error: error)
            return;
        }
        else if accessToken == nil{
            self.alert(message: fbsdkLoginError, title: "Facebook Login Error");
            return
        }
        
        print("Facebook Login: successfully logged in.\nChecking with Database..");
        databaseLogin(accessToken: accessToken!);
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        PFUser.logOutInBackground();
    }
    
    
    @IBAction func testLoginButton(_ sender: UIButton){
        sender.isEnabled = false
        
        let loginHandler = { (action:UIAlertAction, tf1:UITextField?, tf2:UITextField?) in
            sender.isEnabled = true
            print("[Login View Controller] test login...")
            if let username = tf1?.text, let pass = tf2?.text{
                self.databaseLogin(username: username,
                                   password: pass)
            }
        }
        let cancelHandler = { (action:UIAlertAction) in
            sender.isEnabled = true
        }
        
        loginAlert(message: "Enter  credientials",
                   title: "Test Login",
                   actionTitle: "Login",
                   okHandler: loginHandler,
                   cancelHandler: cancelHandler)
    }
    
    // MARK: - Parse Login
    func databaseLogin(accessToken:FBSDKAccessToken) {
        // Facebook Login using Parse Server
        self.showPersistantAlert(message: "Loading Profile...", viewcontroller: self)
        
        PFFacebookUtils.logInInBackground(with: accessToken, block: { (user, pffacebookError) -> Void in
            self.userIsNew = false;
            
            guard let user = user, pffacebookError == nil else{
                // Login Failed
                self.dismissPersistantAlert()
                self.alertForError(error: pffacebookError, title: "Facebook auth login failed")
                return
            }
            
            // Login was successful
            // Bind installation with login 
            self.updateInstallation(loggedInUser: user)
            
            if user.isNew {         // User is new!
                assert(PFUser.current() != nil);    print("[login] -new user from facebok");
                self.userIsNew = true;
                
                self.loadDataFromFacebook(dataFields: self.fbsdkUserFields, user: PFUser.current()!)
            }
            else { // User is same old
                assert(PFUser.current() != nil);    print("[login] -db user logged in through facebook!");
                self.userIsNew = false
                self.loginToLayerSegue()
            }
            
        });
    }
    
    func databaseLogin(username:String, password:String, completion:((Bool)->Void)? = nil)
    {
        self.showPersistantAlert(message: "Logging into database..", viewcontroller: self)
        PFUser.logInWithUsername(inBackground: username, password: password) { (user:PFUser?, error:Error?) in
            guard let loggedInUser = user, error == nil else{
                self.dismissPersistantAlert()
                
                let description = error?.localizedDescription ?? "no description"
                print("[Login View Controller] login error ", description)
                
                self.alertForError(error: error, title: "Login Error")
                return
            }
            
            // Login was successful
            // Bind installation with login
            self.updateInstallation(loggedInUser: loggedInUser)
        
            // Layer Login
            self.loginToLayerSegue()
        }
    }
    
    func loadDataFromFacebook(dataFields:String, user:PFUser){
        print("[login] loading data from facebook...");
        showPersistantAlert(message: "Loading Facebook Profile", viewcontroller: self);
        let request = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": dataFields])
        
        // Start Facebook Graph Request
        request!.start(completionHandler: { (connection, result, error) in
            guard error == nil else{
                self.dismissPersistantAlert()
                
                self.alertForError(error: error, title: "Facbook Error Loading Data:");
                return;
            }
            
            // Result is a dictionary with the user's Facebook data
            let userData:NSDictionary = result as! NSDictionary
            
            print("User Facebook Dump:\t"); print(userData);
            
            if let name = userData["name"] as? String{
                user["name"] = name;
            }
            if let email = userData["email"] as? String{
                user["email"] = email;
            }
            if let location = (userData["location"] as? NSDictionary)?["name"] as? String{
                user["location"] = location;
            }
            if let gender = userData["gender"] as? String{
                user["gender"] = gender;
            }
            if let birthday = userData["birthday"] as? String{
                user["birthday"] = birthday;
            }
            if let relationship = userData["relationship_status"] as? String{
                user["relationship"] = relationship;
            }
            if let about = userData["about"] as? String{
                user["about"] = about;
            }
            if let coverPhoto = (userData["cover"] as? NSDictionary)?["source"] as? String{
                user["additionalPhotosURL"] = [coverPhoto];
            }
            if let facebookID = userData["id"] as? String{
                // If we have the facebook pic url then donwnload and go from here
                
                let fbkpicurl:String = "https://graph.facebook.com/" + facebookID + "/picture?type=large&return_ssl_resources=1";
                let pictureURL:URL = URL(string: fbkpicurl)!;
                
                user["facebookID"] = facebookID;
                user["facebookPictureURL"] = fbkpicurl;
                let userId = user.objectId ?? "ObjID_DoesNotExist";
                
                print("Fetching Profile Pic...");
                // Fetch Url
                self.downloadedFrom(url: pictureURL, completionHandler: { (downloadedImageData:Data?) in
                    // Downloaded Image is here
                    if let downloadedImageData = downloadedImageData {
                        if let _ = user["profilePic"] as? PFFile{
                            // Profile Pic already exists
                            print("[login] profile pic already exists")
                        }
                        else{
                            let fname = "\(userId).jpg"
                            let profilePictureFile = PFFile(name: fname, data: downloadedImageData)
                            print("[login] set facebook profile pic")
                            user["profilePic"] = profilePictureFile
                        }
                    }
                    
                    let defaultSM = ["facebook","instagram","snapchat","whatsapp","twitter"]
                    user["currentSocialMedia"] = defaultSM
                    user.addUniqueObject("facebook", forKey: "dareEntitlements")
                    user.addUniqueObject("facebook", forKey: "publicEntitlements")
                    
                    let userFacebookName:String = user["name"] as? String ?? "N/A"
                    user[kUserSocialMedia1] = userFacebookName
                    
                    user.saveInBackground(block: { (result:Bool, dberror:Error?) in
                        self.dismissPersistantAlert();
                        
                        if (result == true && dberror == nil){
                            self.loginToLayerSegue()
                        }
                        else{
                            self.alertForError(error: dberror, title: "Could not update user data:");
                        }
                    })
                    
                })
                
                return;
            }
        
            // We failed to get Facebook ID so it is an automatic Error
            self.alertForError(error: nil, title: "missing critical facebook data");
        }); // End of Facebook Graph Request
    }
    
    func updateInstallation(loggedInUser:PFUser) -> Void
    {
        // Attach to installation
        if let installation = PFInstallation.current(){
            installation["user"] = loggedInUser
            installation.saveInBackground()
            print("[app-delegate]: binded user with installation")
        }
        else{
            print("[app-delegate]: error missing installation. cannot bind user")
        }
        return
    }
    
    // MARK: Helpers
    private func downloadedFrom(url: URL, completionHandler:@escaping ((Data?) -> Void) ) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let pictureData = data, error == nil
                else {
                    completionHandler(nil);
                    return;
                }
            
                // Image Available Here
                completionHandler(pictureData);
            
            }.resume()
    }
    
    func loginAlert(message: String, title: String = "", actionTitle:String = "ok" ,okHandler: @escaping ((UIAlertAction, UITextField?, UITextField?) -> Void), cancelHandler:((UIAlertAction) -> Void)? = nil)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: actionTitle, style: .default, handler: { (action) in
            let firstTextField:UITextField? = alertController.textFields?.first
            let secondTextField:UITextField? = alertController.textFields![safe: 1]
            okHandler(action, firstTextField, secondTextField)
            return;
        });
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: cancelHandler)
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Username"
        }
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.text = "1234"
            textField.placeholder = "Password"
            textField.clearButtonMode = .always
        }
        
        
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil);
    }
    
    // MARK: Segue
    func loginToLayerSegue(options:[String]? = nil) -> Void{
        self.performSegue(withIdentifier: LayerLoadingSegueId, sender: self);
    }
}

extension LoginViewController{
    func showPersistantAlert(message:String, title:String = "", viewcontroller:UIViewController)
    {
        self.refreshControl.startAnimating()
        
        self.loadingLabel.isHidden = false;
        self.loadingLabel.text = message;
        self.loginButton.isHidden = true;
    }
    
    func dismissPersistantAlert(){
        self.refreshControl.stopAnimating()
        
        self.loadingLabel.isHidden = true;
        self.loadingLabel.text = nil;
        self.loginButton.isHidden = false;
    }
}


extension UIViewController {
    
    func alertForError(error:Error?, title:String = "Error", alternativeMessage:String = "no description")
    {
        let alertController = UIAlertController(title: title, message: error?.localizedDescription ?? alternativeMessage, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .destructive, handler: nil)
        
        alertController.addAction(OKAction);
        self.present(alertController, animated: true, completion: nil);
    }
    
    func alertWith(error:Error?, title:String="Error", alternativeMessage:String="uknown", handler:@escaping ((UIAlertAction) -> Void))
    {
        let alertController = UIAlertController(title: title, message: error?.localizedDescription ?? alternativeMessage, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Dismiss", style: .destructive, handler: handler)
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil);
    }
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil);
    }
    
    /// Shows simple message title alert then gives options to receive tap callbacks 
    func alert(message: String, title: String? = nil, actionTitle:String = "Ok" , successHandler:@escaping ((UIAlertAction) -> Void), cancelTitle:String = "Cancel", cancelHandler:((UIAlertAction) -> Void)? = nil)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let OKAction = UIAlertAction(title: actionTitle, style: .destructive, handler: successHandler)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler)
        
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil);
    }
    
    func showActionSheet(message: String? = nil, title: String = "", actions:[UIAlertAction]? = nil)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet);
        
        if (actions == nil){
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)
        }
        else{
            actions!.forEach({ (anAction) in
                alertController.addAction(anAction);
            });
        }
        self.present(alertController, animated: true, completion: nil);
    }
    
    func errorFor(description:String) -> Error
    {
        let userInfo: [String : String] = [ NSLocalizedDescriptionKey :  description,
                                            NSLocalizedFailureReasonErrorKey : description]
        let err = NSError(domain: "com.Dare.ios", code: 401, userInfo: userInfo)
        
        return err
    }
}

extension Collection where Indices.Iterator.Element == Index {
    
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
