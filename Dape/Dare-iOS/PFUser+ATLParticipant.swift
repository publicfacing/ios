import Foundation
import Parse
import Atlas

extension PFUser:ATLParticipant
{
    /**
     @abstract The full name of the participant as it should be presented in the user interface.
    */
    public var displayName:String {
        return "\(self.firstName) \(self.lastName)"
    }
    
    public var firstName:String {
        guard let name = self["name"] as? String else{
            return ""
        }
        
        let fName = name.components(separatedBy: " ")
        return fName.first ?? ""
    }

    public var lastName:String{
        guard let name = self["name"] as? String else{
            return ""
        }
        
        let lName = name.components(separatedBy: " ")
        if lName.count > 2{
            let lastName:String = lName[1]
            return lastName
        }
        return ""
    }
    
    public var participantIdentifier: String {
        return objectId!
    }
    
    public var userID: String{
        return objectId!
    }

    public var avatarImageURL: URL? {
        return nil
    }

    public var avatarImage: UIImage? {
        return nil
    }
    
    public var avatarInitials: String?{
        let f = getFirstCharacter(value: firstName)
        let l = getFirstCharacter(value: lastName)
        return "\(f)\(l)"
    }
    
    private func getFirstCharacter(value: String) -> Character {
        return value.characters.first ?? "-";
    }
}

//MARK: - Array Extension Tools
protocol Copying {
    init(original: Self)
}

extension Copying {
    func copy() -> Self {
        return Self.init(original: self)
    }
}

extension Array where Element: Copying {
    func clone() -> Array {
        var copiedArray = Array<Element>()
        for element in self {
            copiedArray.append(element.copy())
        }
        return copiedArray
    }
}
