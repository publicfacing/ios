//
//  ConversationFetchViewController.swift
//  Competish
//
//  Created by Utkarsh Kumar on 11/28/15.
//  Copyright © 2015 Kanpurwala. All rights reserved.
//

import UIKit
import Atlas
import LayerKit
import Parse

class ConversationFetchViewController: UIViewController {
    /* MARK: Public API
    var groupObject:PFObject!
    
    var layerClient: LYRClient{
        return Helper.layerClient;
    }
    
    var groupConversation:LYRConversation!
    
    // UI Component
    //@IBOutlet weak var chatViewControllerView: UIView
    var containerView: UIView!
    
    // Load UI Loading Component
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        assert(self.groupObject != nil, "Group public var must be set before presentation");
        
        // Activity Indicator
        
        // Query for the group conversation
        let conversationQuery = LYRQuery(queryableClass:LYRConversation.self)
        conversationQuery.limit = 2;
        conversationQuery.offset = 0;
        
        // Constrain to group chat stored on parse
        let conversationUrlString:String = self.groupObject["chatUrl"] as! String;
        conversationQuery.predicate = LYRPredicate(property: "identifier", predicateOperator: .IsEqualTo, value: conversationUrlString);
        
        let layerClient = Helper.layerClient;
        let conversations = try? layerClient.executeQuery(conversationQuery)
        
        // Don'y grey out pictures
        layerClient.autodownloadMIMETypes = NSSet(objects: "image/jpeg", "image/png", "image/gif",
                                                  ATLMIMETypeImagePNG, ATLMIMETypeImageJPEG, ATLMIMETypeLocation,
                                                  ATLMIMETypeImageGIF) as Set<NSObject>;
        layerClient.autodownloadMaximumContentSize = 1024 * 200;
        
        if let foundConversation = conversations?.firstObject as? LYRConversation{
            groupConversation = foundConversation;
            
            //self.embedChatViewController(groupConversation)
            self.presentControllerWithConversation(groupConversation);
        }
        else{
            Helper.showSingleActionAlertController("Oops", message: "No Group Chat Found\nLogin to the app again", actionTitle: "Retry", viewController: self, actionHandler: { (ACTION) -> Void in
                self.dismissViewControllerAnimated(true, completion: nil)
            })
        }
    }
    
    
    // MARK: - Presentation
    // The following method handles presenting the correct `ConversationViewController`, regardeless of the current state of the navigation stack.
    func presentControllerWithConversation(conversation: LYRConversation)
    {
        let conversationViewController: ConversationViewController = ConversationViewController(layerClient: self.layerClient)
        conversationViewController.displaysAddressBar = false
        conversationViewController.conversation = conversation
        conversationViewController.shouldDisplayAvatarItemForAuthenticatedUser = true
        conversationViewController.shouldDisplayAvatarItemForOneOtherParticipant = true
        //conversationViewController.group = self.groupObject
        
        if self.navigationController!.topViewController == self {
            self.navigationController!.pushViewController(conversationViewController, animated: true)
        } else {
            var viewControllers = self.navigationController!.viewControllers
            let listViewControllerIndex: Int = self.navigationController!.viewControllers.indexOf(self)!
            viewControllers[listViewControllerIndex + 1 ..< viewControllers.count] = [conversationViewController]
            self.navigationController!.setViewControllers(viewControllers, animated: true)
        }
    }
    func embedChatViewController(conversation: LYRConversation)
    {
        let childViewController: ConversationViewController = ConversationViewController(layerClient: self.layerClient)
        childViewController.displaysAddressBar = true
        childViewController.conversation = conversation
        childViewController.group = self.groupObject
        
        self.addChildViewController(childViewController)
        //conversationViewController.view.frame = CGRectMake(0, 0, chatViewControllerView.frame.width, chatViewControllerView.frame.height);
        
        self.view.addSubview(childViewController.view)
        
        childViewController.didMoveToParentViewController(self)
        
    }
    
    // MARK: - Navigation
    @IBAction func segueChat(sender: UIButton) {
        self.presentControllerWithConversation(self.groupConversation)
    }
    */
}


