//
//  DareInterfaceViewController.swift
//  Dare-iOS
//
//  Created by Utkarsh Kumar on 2/25/17.
//  Copyright © 2017 Epoke. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class DareInterfaceViewController: UIViewController {
    public var activity:PFObject!

    fileprivate let CellIdentifierDareSMInfo:String = "Dare_cell"
    @IBOutlet weak var mainPictureView: PFImageView!
    @IBOutlet weak var headerTitleLabel:UILabel!
    
    @IBOutlet weak var tableView:UITableView!
    /// User's Dare Info
    fileprivate var dareInfo:[(sm:String, info:String)]!
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assert(activity != nil, "NSParameter Assert")
        let type = activity["type"] as! String
        assert(type == "dare", "NSParameter Assert: Activity must of type dare")
        let createdBy:PFUser = activity["createdBy"] as! PFUser
        assert(createdBy.isDataAvailable, "Fetch user assocaited with this dare")
        
        // Do any additional setup after loading the view.
        setNeedsStatusBarAppearanceUpdate()
        
        //mainPictureView.contentMode = .scaleAspectFit
        mainPictureView.clipsToBounds = true
        mainPictureView.image = #imageLiteral(resourceName: "Empty Image")
        mainPictureView.file = createdBy["profilePic"] as? PFFile
        mainPictureView.loadInBackground()
        
        // User name
        let fullname = createdBy["name"] as? String
        if let firstName =  fullname?.components(separatedBy: " ").first{
            headerTitleLabel.text = "\(firstName) shared just with you"
        }
        else{
            headerTitleLabel.text = "\(fullname ?? "-") shared just with you"
        }
        
        // Disable table cell selection
        tableView.allowsSelection = false
        
        // Dare Info extraction
        dareInfo = [];
        let empty = [String]()
        let dareEntitlements:[String]! = createdBy["dareEntitlements"] as? [String] ?? empty
        
        guard let currentSocialMedia = createdBy["currentSocialMedia"] as? [String] else{
            return;
        }
        
        for (index, sm) in currentSocialMedia.enumerated(){
            if index >= 5{
                print("[profile] Warning cannot have more than 5 sm in 'currentSocialMedia'")
                continue
            }
            
            let smKey = kUserSocialMediaList[index]
            if let smInfo = createdBy[smKey] as? String, dareEntitlements.contains(sm){
                dareInfo.append((sm: sm, info: smInfo))
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        mainPictureView.image = nil
        mainPictureView.file = nil
        mainPictureView.loadInBackground()
    }
    
    //MARK: - Button Actions
    @IBAction func dismissScreen(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismissDareTableView(_ sender: UIButton) {
        tableView.isHidden = true
    }
    
    @IBAction func showDareTableView(_ sender: UIButton) {
        tableView.isHidden = false
    }
}

extension DareInterfaceViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dareInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifierDareSMInfo, for: indexPath) as! StaticSocialMediaCell
        let tuple = dareInfo[indexPath.row]
        cell.setAccountType(type: tuple.sm)
        cell.setAccountInfo(info: tuple.info)
        return cell
    }
    
}
