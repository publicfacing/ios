import Foundation
import Parse

class UserManager {
    static let sharedManager = UserManager()
    var userCache = NSCache<NSString,PFUser>()

    // MARK Query Methods
    /* 
    func queryForUserWithName(searchText: String, completion: @escaping (([PFUser]?, Error?) -> Void)) {
        let query:PFQuery = PFUser.query()!
        query.whereKey("objectId", notEqualTo: PFUser.current()!.objectId!)
        
        query.findObjectsInBackground { (objects, error) in
            var contacts = [PFUser]()
            if let users = objects as? [PFUser]{
                for user in users
                {
                    if user.displayName.range(of: searchText, options: NSString.CompareOptions.caseInsensitive) != nil{
                        contacts.append(user)
                    }
                }
            }
            completion(contacts, error)
        }
    }
 
    
    func queryForAllUsersWithCompletion(completion: (([PFUser]?, Error?) -> Void)?) {
        let query = PFUser.query()!
        query.whereKey("objectId", notEqualTo: PFUser.current()!.objectId!)
        
        query.findObjectsInBackground { (objects, error) in
            if let users = objects as? [PFUser]{
                completion?(users, error)
            }
            else{
                completion?(nil, error)
            }
        }
    }
     */
    
    /// Synchronus Query Warning
    func queryAndCache(userIDs: [String]) throws -> [PFUser]?
    {
        if Thread.isMainThread{
            print("[User Manager] dont call on main thread.")
        }
        
        let query: PFQuery! = PFUser.query()
        query.whereKey("objectId", containedIn: userIDs)
        
        let found = try query.findObjects()
        let foundUsers = found as! [PFUser]
        
        for aUser in foundUsers {
            self.cacheUserIfNeeded(user: aUser)
        }
        return foundUsers
    }
    
    func queryAndCacheUsersWithIDs(userIDs: [String], completion: (([PFUser]?, Error?) -> Void)?) {
        let query: PFQuery! = PFUser.query()
        query.whereKey("objectId", containedIn: userIDs)
    
        query.findObjectsInBackground(block: {objects, error in
            if let users = objects as? [PFUser]{
                for aUser in users {
                    self.cacheUserIfNeeded(user: aUser)
                }
            
                completion?(users, error)
                return
            }
        
            completion?(nil, error)
            return
        });
    }
    
    func cachedUserForUserID(userID: String) -> PFUser? {
        let userIDString = userID as NSString
        
        if self.userCache.object(forKey: userIDString) != nil {
            return self.userCache.object(forKey: userIDString)
        }
        return nil
    }
    
    private func cacheUserIfNeeded(user: PFUser) -> Void{
        let userId:NSString = user.objectId! as NSString
        if self.userCache.object(forKey: userId) == nil {
            self.userCache.setObject(user, forKey: userId)
        }
    }
    
    /// Synchronus Query Warning
    func cache(users: [PFUser]) -> Bool{
        if users.count < 1{
            return false
        }
        
        assert(users.first!.isDataAvailable)
        for (user) in users{
            self.cacheUserIfNeeded(user: user)
        }
        
        return true
    }
    
    /*
    func unCachedUserIDsFromParticipants(participants: [String]) -> [String]
    {
        assert(PFUser.current() != nil, "Current user must exist for this operation")
        var array = [String]()
        
        let currentUserId:String = PFUser.current()!.objectId!
        for userid in participants {
            if userid == currentUserId{
                // Current User's ID
                continue
            }
            
            let nsstringId = userid as NSString
            if self.userCache.object(forKey: nsstringId) == nil{
                array.append(userid)
            }
        }
        
        return array
    }
    
    func resolvedNamesFromParticipants(participants: [String]) -> [String] {
        var names = [String]()
        for userID: String in participants {
            if (userID == PFUser.current()!.objectId!) {
                continue
            }
            let userIdNSString = userID as NSString
            if let foundUser = self.userCache.object(forKey: userIdNSString){
                names.append(foundUser.firstName)
            }
        }
        return names
    }
    */
}
