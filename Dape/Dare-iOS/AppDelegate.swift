//
//  AppDelegate.swift
//  Dape-iOS
//
//  Created by Utkarsh Kumar on 12/3/16.
//  Copyright © 2016 Dare. All rights reserved.
//  Dare Blue Hex: #001fb5

import UIKit
import Parse
import Bolts
import FBSDKLoginKit
import ParseFacebookUtilsV4
import FBSDKCoreKit
import GoogleMaps
import GooglePlaces
import LayerKit

//MARK: Private API
private let ParseApplicationID:String = "BetDAREiiaig5uxPY68P"
private let ParseClientKey:String = "HPE2ylbFh&HFAHgfiH@nAU"
private var ParseRemoteServerUrl:String = "https://dare-server.herokuapp.com/parse"
private let GoogleMapsAPIKey:String = "AIzaSyA4hV64YA3eIbi4zKj2lNUdaF8raBlkIEg"
private let GooglePlacesAPIKey:String = "AIzaSyA4hV64YA3eIbi4zKj2lNUdaF8raBlkIEg"
//"AIzaSyAbprjQOpzvvhfwbwhg9nj6TeD0MlNfAhg";
private let LayerApplicationStagingID:String = "layer:///apps/staging/c1176e86-e785-11e6-afcf-c4196c035e12"
private let LayerConnectScreeenId = "LayerChatConenctController"

/// Cutoff all Query older than 3 days
let kQueryCutoffDayLimit:Int = -3
let kSecondsInADay:Int = 86400
let kQueryCutoffSecondsLimit:TimeInterval = Double(kQueryCutoffDayLimit) * Double(kSecondsInADay)

let DapeURL:String = "https://dapetheapp.com"
let appStoreAppID = "id706081574"
let DapeAppStoreURL = "itms-apps://itunes.apple.com/app/id" + appStoreAppID

// MARK: Push Related Structs
enum ParsePushType:String {
    case checkin = "user-check-in"
    case checkout = "user-check-out"
    case dare = "user-dare"
}

// Disable console log in live app
#if !arch(x86_64) && !arch(i386)
    public func debugPrint(items: Any..., separator: String = " ", terminator: String = "\n") {
        
    }
    public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
        
    }
#endif

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{

    var window: UIWindow?
    public var layerClient:LYRClient!
    public var layerClientIsInitialized:Bool = false
    
    //MARK: Push Related
    public var deviceToken:Data?
    public var ignoreNotifications:Bool = false
    static let layerPushNotification = Notification.Name("Layer_Push_Notification")
    static let serverPushNotification = Notification.Name("Parse_Push_Notification")
    static let kParseActivityInfoKey = "activity"

    //MARK: - Lifecycle
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        var parseServerUrl:String
        // Testing setup
        if UIDevice.isSimulator{
            parseServerUrl = "http://localhost:1337/parse"
        }
        else{
            parseServerUrl = ParseRemoteServerUrl
        }
        //parseServerUrl = ParseRemoteServerUrl
        
        // Parse Config
        Parse.enableLocalDatastore();
        
        // Initialize Parse.
        let configuration = ParseClientConfiguration {
            $0.applicationId = ParseApplicationID
            $0.server = parseServerUrl
            $0.clientKey = ParseClientKey
            $0.isLocalDatastoreEnabled = true
        }
        Parse.initialize(with: configuration);
        PFAnalytics.trackAppOpened(launchOptions: launchOptions);
        
        print("[app-d] connecting to \(parseServerUrl) ...")
        
        //Init layer
        let layerApiUrl = URL(string: LayerApplicationStagingID)!
        layerClient = LYRClient(appID: layerApiUrl, delegate: self, options: nil)
        layerClientIsInitialized = true
        
        // More Config...
        // Register Parse Subclasses Here
        
        // Register for Push Notitications
        print("[app-d] layer client connecting ...")
        layerClient.connect { (connected, error) in
            print("[app-d] registering for push notifications ...")
            self.registerForPushNotifications(application: application, launchOptions: launchOptions)
        }
        
        // UI Setup
        UIApplication.shared.statusBarStyle = .default
 
        // Facebook Setup:
        PFFacebookUtils.initializeFacebook(applicationLaunchOptions: launchOptions);
        
        // Google Setup:
        GMSServices.provideAPIKey(GoogleMapsAPIKey)
        GMSPlacesClient.provideAPIKey(GooglePlacesAPIKey)
        let appCache = URLCache(memoryCapacity: 40 * 1024 * 1024,
                                diskCapacity: 100 * 1024 * 1024,
                                diskPath: nil)
        URLCache.shared = appCache
        
        // Initial View Controller
        if let currentUser = PFUser.current(){
            print("[app-d] user logged in")
            if let installation = PFInstallation.current(){
                installation["user"] = currentUser
                installation.saveInBackground()
            }
            else{
                print("[app-d]: error missing installation. cannot bind user")
            }
            
            // Navigate to root view controller?
            if let notificationPayload = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary{
                // Notification routing
                let handled = appDidOpen(fromPayload: notificationPayload)
                if handled {
                    // Done Return Early
                    let facebookSDKInstance:FBSDKApplicationDelegate = FBSDKApplicationDelegate.sharedInstance()
                    let facebookStatus = facebookSDKInstance.application(application, didFinishLaunchingWithOptions: launchOptions)
                    return facebookStatus
                }
            }
            
            // Default app routing:
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let refreshscreen = mainStoryboard.instantiateViewController(withIdentifier: LayerConnectScreeenId) as! LayerChatConnectViewController
            window?.rootViewController = refreshscreen
            window?.makeKeyAndVisible()
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func registerForPushNotifications(application: UIApplication, launchOptions: [UIApplicationLaunchOptionsKey: Any]?)
    {
        if application.applicationState != UIApplicationState.background{
            let preBackgroundPush = !application.responds(to: #selector(getter: UIApplication.backgroundRefreshStatus))
            let oldPushHandlerOnly = !self.responds(to: #selector(UIApplicationDelegate.application(_:didReceiveRemoteNotification:fetchCompletionHandler:)))
            var pushPayload = false
            if let options = launchOptions {
                pushPayload = options[UIApplicationLaunchOptionsKey.remoteNotification] != nil
            }
            if (preBackgroundPush || oldPushHandlerOnly || pushPayload) {
                PFAnalytics.trackAppOpened(launchOptions: launchOptions)
            }
        }
        if application.responds(to: #selector(UIApplication.registerUserNotificationSettings(_:))){
            let setting = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(setting)
            application.registerForRemoteNotifications()
        }
        else{
            application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil));
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }

    //MARK: - Facebook
    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool{
        return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
    }
    
    //MARK: - Notifications
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        print("[app-d] memory warning recevied")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        self.deviceToken = deviceToken
        /*  .......   General Handler for Parse CUSTOM  PUSH  .......   */
        guard let installation = PFInstallation.current() else{
            print("[app-d] warning:",
                  "installation does not exist for this device yet")
            return
        }
        
        installation.setDeviceTokenFrom(deviceToken)                            // Set Device Token

        // Current User
        if let currentUser = PFUser.current(){
            installation["user"] = currentUser;
        }
        else{
            installation.remove(forKey: "user");
        }
        
        // Add to Master Channel
        installation.addUniqueObject("Main", forKey: "channels");
        installation.saveEventually()
        
        print("[app-d] - installation:", installation.objectId ?? "missing install id?");
        /*  .......   General Handler for Parse CUSTOM  PUSH  .......   */
        print("[app-d] 📱 Registered for Server Push!")
        
        /* *** Send device token to Layer so Layer can send pushes to this device. *** */
        /*
         TIP: The updateRemoteNotificationDeviceToken will only work if you're connected to Layer. So if you're calling `updateRemoteNotificationDeviceToken` but you're not seeing a message "Push: registered apns device..." in the logs you are probably not connected to Layer. The easiest way to fix this is to move the connectWithCompletion call to inside `didFinishLaunchingWithOptions`. If the client isn’t authenticated we won’t push up the token, but we will still store it. When the client authenticates and starts messaging services then the Layer SDK pushes the stored token up again.
         */
        assert(self.layerClient != nil, "The Layer client has not been initialized!")
        assert(self.layerClient.isConnected, "The Layer client MUST be connected!")
        
        do {
            try self.layerClient.updateRemoteNotificationDeviceToken(deviceToken)
            print("[app-d] application registered for LAYER notifications")
        }
        catch{
            print("[app-d] error: layer notif \(error.localizedDescription)")
        }
        
        var token:String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        print("[app-d] device token \(token)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        if (error as NSError).code == 3010 {
            print("[app-d] ignoring push registeration on the simulator")
            return
        }
        print(error.localizedDescription);
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        let parsePush:Bool = userInfo["layer"] == nil
        //let layerPush:Bool = userInfo["layer"] != nil
        let appIsInactive:Bool = application.applicationState == UIApplicationState.inactive
        
        if appIsInactive{       // Apps woken up from push
            let pushPayload = userInfo as NSDictionary
            // App launched from background
            let handled = appDidOpen(fromPayload: pushPayload)
            if handled{
                completionHandler(.noData)
                return
            }
        }
        
        // Handle Push Notifications:
        if parsePush{   // Parse
            let type:String? = userInfo["type"] as? String;
            if ignoreNotifications == false{
                PFPush.handle(userInfo)
            }
            else{
                print(#function, "warning: notification dropped")
                ignoreNotifications = false     // reset it back to default
            }
            
            completionHandler(.newData)
            if application.applicationState == UIApplicationState.inactive{
                PFAnalytics.trackAppOpened(withRemoteNotificationPayload: userInfo)
            }
            else{
                let parseParams:[String:String?] = [AppDelegate.kParseActivityInfoKey: type]
                print("[app-d] server push received. posting app wide")
                // Post app wide notification
                NotificationCenter.default.post(name: AppDelegate.serverPushNotification,
                                                object: nil,
                                                userInfo: parseParams)
            }
            return
        }
        else{
            let layerUserInfo = userInfo as? [String:Any]
            let convoid:String? = self.conversationIdentifier(FromRemoteNotification: layerUserInfo)
            print("[app-d] layer push parsed. posting app wide")
            let layerParams:[String:String?] = ["conversation_id": convoid]
            layerClient.synchronize(withRemoteNotification: userInfo, completion: { (convo, message, error) in
                print("Layer push synchronized")
            })
            
            // Post app wide notification
            NotificationCenter.default.post(name: AppDelegate.layerPushNotification,
                                            object: nil,
                                            userInfo: layerParams)
            completionHandler(.noData)
        }
    }
    
    //MARK: - Navigation
    func appDidOpen(fromPayload payload: NSDictionary) -> Bool{
        var defaultTab:HomeViewControllerTabs
        if let ntype = payload["type"] as? String, let notificationType = ParsePushType(rawValue: ntype){
            // we got a notification of recognized type
            if notificationType == .checkin || notificationType == .checkout{
                defaultTab = .people
            }
            else if notificationType == .dare{
                defaultTab = .flow
            }
            else{
                return false; // dont handle
            }
        }
        else if payload["layer"] != nil{
            defaultTab = .flow
        }
        else{
            return false; // dont handle
        }
        
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let dvc = storyboardMain.instantiateViewController(withIdentifier: LayerConnectScreeenId) as! LayerChatConnectViewController
        dvc.shouldSelectHomeTab = defaultTab
        
        window?.rootViewController = dvc
        window?.makeKeyAndVisible()
        return true
    }
    
    
    //MARK: Layer Push Helpers
    func getBackgroundFetchResult(changes: [AnyObject]!, error: NSError!) -> UIBackgroundFetchResult {
        if changes.count > 0 {
            return UIBackgroundFetchResult.newData
        }
        return error != nil ? UIBackgroundFetchResult.failed : UIBackgroundFetchResult.noData;
    }
    
    func conversationIdentifier(FromRemoteNotification notification:[String:Any]?) -> String?
    {
        if let layerMap = notification?["layer"] as? [String:String]{
            guard let convoid = layerMap["conversation_identifier"] else{
                print("[app-d] error - layer push has no key 'conversation_identifier'");
                return nil
            }
            return convoid
        }
        else{
            return nil
        }
    }
    
    func conversationFromRemoteNotification(remoteNotification:[String:Any]?) -> LYRConversation?
    {
        if let layerMap = remoteNotification?["layer"] as? [String:String]{
            guard let convoid = layerMap["conversation_identifier"] else{
                print("[app-d] error - layer push has no key 'conversation_identifier'");
                return nil
            }
            
            let conversationIdentifier = URL(string: convoid)!
            return self.existingConversationForIdentifier(identifier: conversationIdentifier)
        }
        else{
            return nil
        }
    }
    
    func navigateToViewForConversation(conversation: LYRConversation){
        print("[app-d] navigate to conversation \(conversation.identifier) here.")
    }
    
    func existingConversationForIdentifier(identifier:URL) -> LYRConversation?
    {
        let query: LYRQuery = LYRQuery(queryableClass: LYRConversation.self)
        query.predicate = LYRPredicate(property: "identifier",
                                       predicateOperator: .isEqualTo,
                                       value: identifier)
        query.limit = 1
        do {
            return try self.layerClient.execute(query).firstObject as? LYRConversation
        } catch {
            print("[app-d] error with layer client.\n\(error.localizedDescription)")
            return nil
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication){
        // Reset Push Badge
        guard let currentInstallation = PFInstallation.current() else{
            print("[app-d] warning missing installation")
            return
        }
        
        if currentInstallation.badge != 0{
            currentInstallation.badge = 0;
            currentInstallation.saveEventually()
        }
        
        FBSDKAppEvents.activateApp()
    }
}

// MARK: - Layer Background Nonce
extension AppDelegate: LYRClientDelegate{
    func internallyJustAnswerTheNonce(nonceToken:String){
        guard let userID = PFUser.current()?.objectId else{
            print("[app-delegate] fatal logic error.",
                  "asking for token when no one is signed in")
            displayAlertOnRootWindow(message: "Logic assert: asking for token for missing current user")
            return
        }
        /*
         * 2. Connect to your backend to generate an identity token.
         */
        print("☁️️ [app-delegate]: requesting cloud generated token")
        PFCloud.callFunction(inBackground: "generateToken", withParameters: ["nonce": nonceToken, "userID": userID], block: { (object:Any?, error:Error?) in
            
            guard let identityToken = object as? String, error == nil else{
                let errmsg = "Dape servers are not issuing valid identity token at this time"
                print("[layer-client]: * authd failed \(errmsg)")
                self.displayAlertOnRootWindow(message: errmsg)
                return;
            }
            
            /*
             * 3. Submit identity token to Layer for validation
             */
            self.layerClient.authenticate(withIdentityToken: identityToken, completion: { (authenticatedUser, lyrerr) in
                if let authUser = authenticatedUser {
                    let id = authUser.userID;
                    print("[layer-client]: * authd as user \(id)")
                }
                else {  // Step 3 Error
                    print("[layer-client]: * authd failed")
                    self.displayAlertOnRootWindow(message: "Authentication for Layer failed.")
                }
            });
        })
    }
    
    func displayAlertOnRootWindow(message:String){
        let alert = UIAlertController(title: "Layer Chat Info",
                                      message: message,
                                      preferredStyle: .alert)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: LYRDelegates
    func layerClient(_ client: LYRClient, didReceiveAuthenticationChallengeWithNonce nonce: String){
        print("[app-d] * received an auth challenge")
        self.internallyJustAnswerTheNonce(nonceToken: nonce)
    }
}

extension AppDelegate{
    func errorFor(key:String, description:String) -> Error
    {
        let userInfo: [String : String] = [
            NSLocalizedDescriptionKey :  NSLocalizedString(key, value: description, comment: ""),
            NSLocalizedFailureReasonErrorKey : NSLocalizedString(key, value: description, comment: "")
        ]
        let err = NSError(domain: "com.Dape.ios", code: 401, userInfo: userInfo)
        
        return err
    }
}


extension UIDevice {
    static var isSimulator: Bool {
        return ProcessInfo.processInfo.environment["SIMULATOR_DEVICE_NAME"] != nil
    }
}


extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
extension Float {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return (self * divisor).rounded() / divisor
    }
}

extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
}



/*
 let userTappedRemoteNotification:Bool = application.applicationState == .inactive
 if userTappedRemoteNotification == true
 {
 //Loading Alert Here
 if let conversation = conversationFromRemoteNotification(remoteNotification: layerUserInfo){
 navigateToViewForConversation(conversation: conversation)
 }
 else{
 print("[app-d] layer error")
 completionHandler(.noData)
 }
 }
 
 let success = layerClient.synchronize(withRemoteNotification: userInfo) { (conversation, message, error) in
 //if userTappedRemoteNotification && conversation == nil {
 if conversation != nil
 {
 if let convoid = self.conversationIdentifier(FromRemoteNotification: layerUserInfo){
 print("[app-d] layer push parsed. posting app wide")
 let params = ["conversation_id": convoid]
 NotificationCenter.default.post(name: AppDelegate.layerPushNotification,
 object: params)
 }
 else{
 print("[app-d] layer push parse error")
 completionHandler(.noData)
 }
 }
 else{
 print("[app-d] layer error")
 completionHandler(.noData)
 }
 }
 
 if success == false{
 completionHandler(.noData)
 }
 */
